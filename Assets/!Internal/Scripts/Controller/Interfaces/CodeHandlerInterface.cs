
using System;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;

using ICD.CashMachine.Model;

namespace ICD.CashMachine.Controller.Interfaces
{
	public interface CodeHandlerInterface
	{
		[Serializable]
		public class HadnlingResult
		{
			public enum Type
			{
				finished,
				expired,
				aborted
			}

			public Type type { get; }
			public float resultScore { get; }

			public HadnlingResult(Type type, float resultScore = 0)
			{
				this.type = type;
				this.resultScore = resultScore;
			}
		}

		public Code currentCode { get; }

		internal Task<HadnlingResult> HandleCode(Code code, InputHandlerInterface inputHandler, CancellationToken cancellationToken);
		internal void AbortCodeHandling();
	}
	internal interface CodeHandlerInterface<CodeTpe> : CodeHandlerInterface
		where CodeTpe : Code
	{
		public Task<HadnlingResult> HandleCode(CodeTpe code, InputHandlerInterface inputHandler, CancellationToken cancellationToken);
	}
}
