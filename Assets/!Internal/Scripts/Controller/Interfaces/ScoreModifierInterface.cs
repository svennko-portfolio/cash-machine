
namespace ICD.CashMachine.Controller.Interfaces
{
	public interface ScoreModifierInterface
	{
		public void ModifyScore(ref float score, CodeHandlerInterface.HadnlingResult.Type handlingResultType);
		public void Reset();
	}
}
