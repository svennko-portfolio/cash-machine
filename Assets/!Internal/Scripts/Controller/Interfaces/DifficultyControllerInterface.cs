
namespace ICD.CashMachine.Controller
{
    public interface DifficultyControllerInterface
    {
        public float currentDifficulty { get; }
        internal void Start();
        internal void Stop();
    }
}
