
using System;

namespace ICD.CashMachine.Controller.Interfaces
{
	public interface ScoreManagerInterface
	{
		public float totalScore { get; }

		public event EventHandler<float> ScoreAdded;
		public event EventHandler<float> TotalScoreChanged;
		internal void AddScore(float addingScore, CodeHandlerInterface.HadnlingResult.Type handlingResultType, bool useModifiers = true);
		public void ResetScore();
	}
}
