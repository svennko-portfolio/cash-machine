﻿
namespace ICD.CashMachine.Controller.Interfaces
{
	interface InputHandlerInterface
	{
		public string currentInput { get; }
		internal void ResetInput();
	}
}
