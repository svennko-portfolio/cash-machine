
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;
using ICD.Engine.Core.Utility;
using CurveJSON = ICD.Engine.Core.Utility.AnimationCurveHelper.Curve;

using ICD.CashMachine.Model;
using ICD.CashMachine.Model.Interfaces;
using ICD.CashMachine.Controller.Interfaces;

namespace ICD.CashMachine.Controller
{
	[CreateAssetMenu(menuName = "ICD/Cash Machine/Code Handlers/Normal Code Handler", fileName = "Normal Code Handler")]
	public sealed class NormalCodeHandler : TypedCodeHandler<NormalCode>, JSONSerializedObjectInterface
	{
		[Serializable]
		public class BaseReward
		{
			[SerializeField]
			private ushort _codeLength = 1;
			public ushort codeLength
			{
				get => _codeLength;
				set => _codeLength = value;
			}

			[SerializeField]
			private float _value = 100;
			public float value
			{
				get => _value;
				set => _value = value;
			}
		}

		[SerializeField]
		private List<BaseReward> _baseRewards;

		[SerializeField]
		private AnimationCurve _rewardMultiplierByNormalizedActivationTime = AnimationCurve.Constant(0, 1, 1);

		[SerializeField]
		private float
			_expirationFine,
			_incorrectInputFine;

		protected override float expirationScore => _expirationFine;
		protected override float skipScore => 0;

		protected sealed override bool IsInputFinished(string input, out float resultScore)
		{
			resultScore = 0;

			if (currentCode.isExpired)
			{
				resultScore = _expirationFine;
				return true;
			}

			if (input.Length < currentCode.value.Length)
				return false;

			if (currentCode.IsMatching(input, out bool fullMathcing))
			{
				resultScore =
					GetBaseRewardByCodeLength(
						(ushort)currentCode.value.Length) *
					_rewardMultiplierByNormalizedActivationTime.Evaluate(
						currentCode.normalizedActivationTime);
				return true;
			}
			else
			{
				resultScore = _incorrectInputFine;
				return true;
			}
		}

		float GetBaseRewardByCodeLength(ushort codeLength)
		{
			foreach (var reward in _baseRewards)
				if (reward.codeLength == codeLength)
					return reward.value;

			var additionalReward =
				new BaseReward()
				{
					codeLength = codeLength
				};
			_baseRewards.Add(
				additionalReward);
			return additionalReward.value;
		}

		#region Serialized Object

		[Serializable]
		struct BaseRewardJSON
		{
			public int codeLength;
			public float value;
		}

		[Serializable]
		struct NormalCodeHandlerJSON
		{
			public BaseRewardJSON[] baseRewards;

			public float
				expirationFine,
				incorrectInputFine;

			public CurveJSON rewardMultiplierByNormalizedActivationTime;
		}

		bool SerializedObjectInterface.SerializeSelf(out object serializedData) =>
			(this as SerializedObjectInterface<string>).SerializeSelf(
				out serializedData);

		bool SerializedObjectInterface.DeserializeSelf(object serializedData) =>
			(this as SerializedObjectInterface<string>).DeserializeSelf(
				(string)serializedData);

		bool SerializedObjectInterface<string>.DeserializeSelf(string serializedData)
		{
			try
			{
				var representation =
					JsonUtility.FromJson<NormalCodeHandlerJSON>(
						serializedData);

				_baseRewards.Clear();
				_baseRewards.AddRange(
					from reward in representation.baseRewards
					select new BaseReward()
					{
						codeLength =
							reward.codeLength < 0 ?
								(ushort)0 :
								(ushort)reward.codeLength,
						value = reward.value
					});

				_rewardMultiplierByNormalizedActivationTime =
					representation.rewardMultiplierByNormalizedActivationTime.curve;

				_expirationFine = representation.expirationFine;
				_incorrectInputFine = representation.incorrectInputFine;

				return true;
			}
			catch (Exception exception)
			{
				Debug.LogException(
					exception);
				return false;
			}
		}

		bool SerializedObjectInterface<string>.SerializeSelf(out string serializedData)
		{
			serializedData = string.Empty;
			try
			{
				serializedData =
					JsonUtility.ToJson(
						new NormalCodeHandlerJSON()
						{
							baseRewards =
								(from reward in _baseRewards
								 select new BaseRewardJSON()
								 {
									 codeLength = reward.codeLength,
									 value = reward.value
								 }).ToArray(),
							rewardMultiplierByNormalizedActivationTime =
								CurveJSON.FromCurve(
									_rewardMultiplierByNormalizedActivationTime),
							expirationFine = _expirationFine,
							incorrectInputFine = _incorrectInputFine
						},
					true);
				return true;
			}
			catch (Exception exception)
			{
				Debug.LogException(
					exception);
				return false;
			}
		}
		#endregion
	}
}
