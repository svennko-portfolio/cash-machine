
using UnityEngine;
using UnityEngine.Events;

using ICD.Engine.Core.Singletons;

using ICD.CashMachine.Controller.Interfaces;

namespace ICD.CashMachine
{
	public class InputHandler : Singleton<InputHandler>, InputHandlerInterface
	{
		[SerializeField]
		private string _currentInput;

		[SerializeField]
		private UnityEvent _hasBeenReset;

		public string currentInput => _currentInput;

		public override bool dontDestroyOnLoad => false;

		public void UpdateInput(string input) =>
			_currentInput = input;

		[ContextMenu("Reset Input")]
		void InputHandlerInterface.ResetInput()
		{
			_currentInput = string.Empty;
			_hasBeenReset?.Invoke();
		}
	}
}
