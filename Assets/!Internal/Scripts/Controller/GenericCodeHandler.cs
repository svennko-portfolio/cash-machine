
using System;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;
using Sirenix.OdinInspector;
using ICD.Engine.Core.CustomAttributes;
using ICD.Engine.Core.Utility;
using CurveJSON = ICD.Engine.Core.Utility.AnimationCurveHelper.Curve;

using ICD.CashMachine.Model;
using ICD.CashMachine.Model.Interfaces;
using ICD.CashMachine.Controller.Interfaces;

namespace ICD.CashMachine.Controller
{
	[CreateAssetMenu(menuName = "ICD/Cash Machine/Code Handlers/Generic Code Handler", fileName = "Generic Code Handler")]
	public class GenericCodeHandler : ScriptableObject, CodeHandlerInterface, DifficultyObjectInterface, JSONSerializedObjectInterface
	{
		[SerializeField]
		private Code _currentCode;
		public Code currentCode => _currentCode;

		[SerializeField]
		[RequireInterface(typeof(CodeHandlerInterface<NormalCode>))]
		[Required]
		private UnityEngine.Object _normalCodeHandler;

		[SerializeField]
		[RequireInterface(typeof(CodeHandlerInterface<WrongCode>))]
		[Required]
		private UnityEngine.Object _wrongCodeHandler;
		private CodeHandlerInterface<NormalCode> normalCodeHandler => _normalCodeHandler as CodeHandlerInterface<NormalCode>;
		private CodeHandlerInterface<WrongCode> wrongCodeHandler => _wrongCodeHandler as CodeHandlerInterface<WrongCode>;

		[SerializeField]
		private float _difficulty = 1;
		public float difficulty => _difficulty;

		[SerializeField]
		private AnimationCurve _expirationTimeByDifficulty;


		private CancellationTokenSource
			_destroyCancellation = new CancellationTokenSource(),
			_handlingCancellation;

		async Task<CodeHandlerInterface.HadnlingResult> CodeHandlerInterface.HandleCode(Code code, InputHandlerInterface inputHandler, CancellationToken cancellationToken)
		{
			if (!code)
				throw
					new ArgumentNullException(
						nameof(code));
			if (inputHandler == null)
				throw
					new ArgumentNullException(
						nameof(code));

			if (_currentCode &&
				!_currentCode.isExpired)
				_currentCode.Abort();

			_handlingCancellation?.Cancel();
			_handlingCancellation = new CancellationTokenSource();
			cancellationToken =
				CancellationTokenSource.CreateLinkedTokenSource(
					cancellationToken,
					_destroyCancellation.Token,
					_handlingCancellation.Token).Token;

			_currentCode = code;

			code.expirationDuration =
				_expirationTimeByDifficulty.Evaluate(
					_difficulty);

			code.Activate(
				cancellationToken);

			CodeHandlerInterface.HadnlingResult result = null;

			switch (code)
			{
				case NormalCode castedCode:
					result =
						await normalCodeHandler.HandleCode(
							castedCode,
							inputHandler,
							cancellationToken);
					break;
				case WrongCode castedCode:
					result =
						await wrongCodeHandler.HandleCode(
							castedCode,
							inputHandler,
							cancellationToken);
					break;
			}

			_handlingCancellation?.Dispose();
			_handlingCancellation = null;

			return result;
		}

		void CodeHandlerInterface.AbortCodeHandling()
		{
			_handlingCancellation?.Cancel();
			_handlingCancellation = null;
		}

		void DifficultyObjectInterface.UpdateDifficulty(float newDifficulty) =>
			_difficulty = newDifficulty;

		void OnDestroy()
		{
			_destroyCancellation?.Cancel();
		}

		#region Serialized Object

		[Serializable]
		struct GenericCodeHandlerJSON
		{

			public CurveJSON expirationTimeByDifficulty;
		}

		bool SerializedObjectInterface.SerializeSelf(out object serializedData) =>
			(this as JSONSerializedObjectInterface).SerializeSelf(
				out serializedData);

		bool SerializedObjectInterface.DeserializeSelf(object serializedData) =>
			(this as JSONSerializedObjectInterface).DeserializeSelf(
				(string)serializedData);

		bool SerializedObjectInterface<string>.DeserializeSelf(string serializedData)
		{
			try
			{
				var representation =
					JsonUtility.FromJson<GenericCodeHandlerJSON>(
						serializedData);

				_expirationTimeByDifficulty =
					representation.expirationTimeByDifficulty.curve;

				return true;
			}
			catch (Exception exception)
			{
				Debug.LogException(
					exception);
				return false;
			}
		}

		bool SerializedObjectInterface<string>.SerializeSelf(out string serializedData)
		{
			serializedData = string.Empty;
			try
			{
				serializedData =
					JsonUtility.ToJson(
						new GenericCodeHandlerJSON()
						{
							expirationTimeByDifficulty =
								CurveJSON.FromCurve(
									_expirationTimeByDifficulty)
						},
					true);
				return true;
			}
			catch (Exception exception)
			{
				Debug.LogException(
					exception);
				return false;
			}
		}
		#endregion
	}
}
