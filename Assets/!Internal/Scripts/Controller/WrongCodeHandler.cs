
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;

using ICD.CashMachine.Model;
using ICD.CashMachine.Model.Interfaces;
using ICD.CashMachine.Controller.Interfaces;

namespace ICD.CashMachine.Controller
{
	[CreateAssetMenu(menuName = "ICD/Cash Machine/Code Handlers/Wrong Code Handler", fileName = "Wrong Code Handler")]
	public sealed class WrongCodeHandler : TypedCodeHandler<WrongCode>, JSONSerializedObjectInterface
	{
		[SerializeField]
		private float _fullInputFine;

		protected override float expirationScore => 0;
		protected override float skipScore => 0;

		protected sealed override bool IsInputFinished(string input, out float resultScore)
		{
			resultScore = 0;

			if (input.Length >= currentCode.value.Length)
			{
				resultScore = _fullInputFine;
				return true;
			}
			else
				return false;
		}

		#region Serialized Object
		[Serializable]
		private struct WrongCodeHandlerJSON
		{
			public float fullInputFine;
		}

		bool SerializedObjectInterface.SerializeSelf(out object serializedData) =>
			(this as SerializedObjectInterface<string>).SerializeSelf(
				out serializedData);

		bool SerializedObjectInterface.DeserializeSelf(object serializedData) =>
			(this as SerializedObjectInterface<string>).DeserializeSelf(
				(string)serializedData);

		bool SerializedObjectInterface<string>.DeserializeSelf(string serializedData)
		{
			try
			{
				_fullInputFine =
					JsonUtility.FromJson<WrongCodeHandlerJSON>(
						serializedData).fullInputFine;
				return true;
			}
			catch (Exception exception)
			{
				Debug.LogException(
					exception);
				return false;
			}
		}

		bool SerializedObjectInterface<string>.SerializeSelf(out string serializedData)
		{
			serializedData = string.Empty;
			try
			{
				serializedData =
					JsonUtility.ToJson(
						new WrongCodeHandlerJSON()
						{
							fullInputFine = _fullInputFine
						},
					true);
				return true;
			}
			catch (Exception exception)
			{
				Debug.LogException(
					exception);
				return false;
			}
		}
		#endregion
	}
}
