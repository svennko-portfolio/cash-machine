
using System;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using ICD.Engine.Core.Singletons;
using ICD.Engine.Core.CustomAttributes;
using ICD.Engine.Core.Utility;
using CurveJSON = ICD.Engine.Core.Utility.AnimationCurveHelper.Curve;

using ICD.CashMachine.Model;
using ICD.CashMachine.Model.Interfaces;

namespace ICD.CashMachine.Controller
{
	public class GameTimer : Singleton<GameTimer>
	{
		[SerializeField]
		[Required]
		[AssetsOnly]
		private SerializedFloatValue _gameDuration;

		[SerializeField]
		private float _currentGameTime;

		[FoldoutGroup("Events")]
		[SerializeField]
		private UnityEvent
			_countingStarted,
			_countingAborted,
			_countingFinished;

		public event EventHandler
			CountingStarted,
			CountingAborted,
			CountingFinished;

		public float gameDuration => _gameDuration;
		public float currentGameTime => _currentGameTime;
		public float normalizedGameTime => _currentGameTime / _gameDuration;

		public override bool dontDestroyOnLoad => false;

		private CancellationTokenSource
			_destroyCancelation = new CancellationTokenSource(),
			_countingCancellation;

		public GameTimer()
		{
			Destroyed += HandleDestroyEvent;

			CountingStarted += HandleCountingStartEvent;
			CountingAborted += HandleCountingAbortationEvent;
			CountingFinished += HandleCountingFinishEvent; ;
		}

		[ContextMenu("Start Counting")]
		public void StartCounting()
		{
			ResetTimer();
			CountTime();
		}

		[ContextMenu("Stop Counting")]
		public void StopCounting()
		{
			_countingCancellation?.Cancel();
			_countingCancellation = null;
		}

		[ContextMenu("Reset Timer")]
		public void ResetTimer()
		{
			StopCounting();
			_currentGameTime = 0;
		}

		async void CountTime(CancellationToken cancellationToken = default)
		{
			_countingCancellation?.Cancel();
			_countingCancellation = new CancellationTokenSource();
			cancellationToken =
				CancellationTokenSource.CreateLinkedTokenSource(
					cancellationToken,
					_destroyCancelation.Token,
					_countingCancellation.Token).Token;

			_currentGameTime = 0;
			_countingStarted?.Invoke();

			for (
					float startTime = Time.time;

					_currentGameTime < _gameDuration;

					_currentGameTime = Time.time - startTime
				)
			{
				if (cancellationToken.IsCancellationRequested)
				{
					FinalizeCounting();
					CountingAborted?.Invoke(
						this,
						EventArgs.Empty);
					return;
				}

				await Task.Yield();
			}

			FinalizeCounting();
			CountingFinished?.Invoke(
				this,
				EventArgs.Empty);
		}

		void FinalizeCounting()
		{
			_countingCancellation?.Dispose();
			_countingCancellation = null;
		}

		void HandleDestroyEvent()
		{
			_destroyCancelation?.Cancel();
		}

		void HandleGameStartEvent(object sender, EventArgs eventArguments)
		{
			StartCounting();
		}

		void HandleGameAborationEvent(object sender, EventArgs eventArguments)
		{
			StopCounting();
		}

		void HandleCountingStartEvent(object sender, EventArgs eventArguments)
		{
			_countingStarted?.Invoke();
		}

		void HandleCountingAbortationEvent(object sender, EventArgs eventArguments)
		{
			_countingAborted?.Invoke();
		}

		void HandleCountingFinishEvent(object sender, EventArgs eventArguments)
		{
			_countingFinished?.Invoke();
		}
	}
}
