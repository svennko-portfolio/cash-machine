
using System;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;

using ICD.CashMachine.Model;
using ICD.CashMachine.Controller.Interfaces;

namespace ICD.CashMachine.Controller
{
	public abstract class TypedCodeHandler<CodeType> : ScriptableObject, CodeHandlerInterface<CodeType>
		where CodeType : Code
	{
		[SerializeField]
		private CodeType _currentCode;
		public Code currentCode => _currentCode;

		protected abstract bool IsInputFinished(string input, out float resultScore);

		protected abstract float expirationScore { get; }
		protected abstract float skipScore { get; }

		void CodeHandlerInterface.AbortCodeHandling()
		{
			_handlingCancellation?.Cancel();
			_handlingCancellation = null;
		}

		private CancellationTokenSource
			_destroyCancellation = new CancellationTokenSource(),
			_handlingCancellation;

		async Task<CodeHandlerInterface.HadnlingResult> CodeHandlerInterface<CodeType>.HandleCode(CodeType code, InputHandlerInterface inputHandler, CancellationToken cancellationToken)
		{
			if (!code)
				throw
					new ArgumentNullException(
						nameof(code));
			if (inputHandler == null)
				throw
					new ArgumentNullException(
						nameof(code));

			if (_currentCode &&
				!_currentCode.isExpired)
				_currentCode.Abort();

			_handlingCancellation?.Cancel();
			_handlingCancellation = new CancellationTokenSource();
			cancellationToken =
				CancellationTokenSource.CreateLinkedTokenSource(
					cancellationToken,
					_destroyCancellation.Token,
					_handlingCancellation.Token).Token;

			_currentCode = code;

			var resultScore = 0f;

			while (code.isActive)
			{
				if (cancellationToken.IsCancellationRequested)
					return
						new CodeHandlerInterface.HadnlingResult(
							CodeHandlerInterface.HadnlingResult.Type.aborted);

				if (IsInputFinished(inputHandler.currentInput, out resultScore))
				{
					FinalizeHandling();
					code.Abort();
					return
						new CodeHandlerInterface.HadnlingResult(
							CodeHandlerInterface.HadnlingResult.Type.finished,
							resultScore);
				}

				await Task.Yield();
			}

			IsInputFinished(
				inputHandler.currentInput,
				out resultScore);
			FinalizeHandling();
			return
				new CodeHandlerInterface.HadnlingResult(
					CodeHandlerInterface.HadnlingResult.Type.expired,
					resultScore);
		}

		void FinalizeHandling()
		{

			_handlingCancellation?.Dispose();
			_handlingCancellation = null;

			_currentCode = null;
		}

		async Task<CodeHandlerInterface.HadnlingResult> CodeHandlerInterface.HandleCode(Code code, InputHandlerInterface inputHandler, CancellationToken cancellationToken)
		{
			var castedCode = code as CodeType;
			if (!castedCode)
				throw new ArgumentException(
					$"Handling code must be type of: {typeof(CodeType)}");

			return await
				(this as CodeHandlerInterface<CodeType>).HandleCode(
					castedCode,
					inputHandler,
					cancellationToken);
		}
	}
}
