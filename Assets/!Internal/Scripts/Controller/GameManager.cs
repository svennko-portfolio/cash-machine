
using System;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using ICD.Engine.Core.CustomAttributes;
using ICD.Engine.Core.Singletons;

using ICD.CashMachine.Model;
using ICD.CashMachine.Model.Interfaces;
using ICD.CashMachine.Controller.Interfaces;

namespace ICD.CashMachine.Controller
{
	public class GameManager : Singleton<GameManager>
	{
		public override bool dontDestroyOnLoad => false;

		[SerializeField]
		[RequireInterface(typeof(CodeGeneratorInterface))]
		[Required]
		UnityEngine.Object _generator;
		private CodeGeneratorInterface codeGenerator => _generator as CodeGeneratorInterface;

		[SerializeField]
		[RequireInterface(typeof(CodeHandlerInterface))]
		[Required]
		UnityEngine.Object _codeHandler;
		private CodeHandlerInterface codeHandler => _codeHandler as CodeHandlerInterface;

		[SerializeField]
		[RequireInterface(typeof(DifficultyControllerInterface))]
		UnityEngine.Object _difficultyController;
		private DifficultyControllerInterface difficultyController => _difficultyController as DifficultyControllerInterface;

		[SerializeField]
		[RequireInterface(typeof(InputHandlerInterface))]
		[Required]
		UnityEngine.Object _inputHandler;
		private InputHandlerInterface inputHandler => _inputHandler as InputHandlerInterface;

		[SerializeField]
		[RequireInterface(typeof(ScoreManagerInterface))]
		UnityEngine.Object _scoreManager;
		private ScoreManagerInterface scoreManager => _scoreManager as ScoreManagerInterface;

		[FoldoutGroup("Events")]
		[SerializeField]
		private UnityEvent
			_gameStarted,
			_gameAborted,
			_gameFinished;

		[Serializable]
		public class CodeEvent : UnityEvent<Code> { }

		[FoldoutGroup("Events")]
		[SerializeField]
		private CodeEvent _newCodeGenerated;

		public event EventHandler
			GameStarted,
			GameAborted,
			GameFinished;

		public Code currentCode => codeHandler?.currentCode;

		CancellationTokenSource
			_destroyCancellation = new CancellationTokenSource(),
			_gameCancellation;
		public bool isPlaying => _gameCancellation != null;

		public GameManager()
		{
			GameStarted += HadnleGameStartEvent;
			GameAborted += HandleGameAbortationEvent;
			GameFinished += HandleGameFinishEvent;
		}

		[ContextMenu("Start Game")]
		public void StartGame()
		{
			AbortGame();
			PlayGame();
		}

		async void PlayGame(CancellationToken cancellationToken = default)
		{
			_gameCancellation?.Cancel();
			_gameCancellation = new CancellationTokenSource();
			cancellationToken =
				CancellationTokenSource.CreateLinkedTokenSource(
					_destroyCancellation.Token,
					_gameCancellation.Token).Token;

			GameStarted?.Invoke(
				this,
				EventArgs.Empty);

			while (true)
			{
				if (cancellationToken.IsCancellationRequested)
					break;

				inputHandler.ResetInput();
				var newCode = codeGenerator.GenerateRandomCode();
				_newCodeGenerated?.Invoke(
					newCode);

				var result = await
					codeHandler.HandleCode(
						newCode,
						inputHandler,
						cancellationToken);

				if (scoreManager != null)
					scoreManager.AddScore(
						result.resultScore,
						result.type);
			}

			_gameCancellation?.Dispose();
			_gameCancellation = null;
		}

		[ContextMenu("Abort Game")]
		public void AbortGame()
		{
			if (isPlaying)
			{
				codeHandler?.AbortCodeHandling();
				GameAborted?.Invoke(
					this,
					EventArgs.Empty);
			}

			_gameCancellation?.Cancel();
			_gameCancellation = null;
		}

		[ContextMenu("Finish Game")]
		public void FinishGame()
		{
			if (isPlaying)
				GameFinished?.Invoke(
					this,
					EventArgs.Empty);

			_gameCancellation?.Cancel();
			_gameCancellation = null;
		}

		[ContextMenu("Skip Current Code")]
		public void SkipCurrentCode()
		{
			if (isPlaying)
				codeHandler.AbortCodeHandling();
		}

		void HadnleGameStartEvent(object sender, EventArgs eventArguments)
		{
			inputHandler?.ResetInput();
			scoreManager?.ResetScore();
			difficultyController?.Start();

			_gameStarted?.Invoke();
		}

		void HandleGameAbortationEvent(object sender, EventArgs eventArguments)
		{
			inputHandler?.ResetInput();
			difficultyController?.Stop();

			_gameAborted?.Invoke();
		}

		void HandleGameFinishEvent(object sender, EventArgs eventArguments)
		{
			inputHandler?.ResetInput();
			difficultyController?.Stop();

			_gameFinished?.Invoke();
		}

		void OnDestroy()
		{
			_destroyCancellation?.Cancel();
		}
	}
}
