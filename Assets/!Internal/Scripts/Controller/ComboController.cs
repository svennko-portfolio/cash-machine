
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using ICD.Engine.Core.CustomAttributes;
using ICD.Engine.Core.Singletons;

using ICD.CashMachine.Model;
using ICD.CashMachine.Controller.Interfaces;

namespace ICD.CashMachine.Controller
{
	[RequireComponent(typeof(ScoreManager))]
	public class ComboController : MonoBehaviour, ScoreModifierInterface
	{
		[SerializeField]
		private float _multiplier = 2;

		[SerializeField]
		[Min(0)]
		[ValidateInput("SetProgressFromInspector")]
		private int _currentProgress;

#if UNITY_EDITOR
		int _validatedProgress;
		bool SetProgressFromInspector(int newProgress)
		{
			if (newProgress != _validatedProgress)
			{
				_currentProgress = _validatedProgress;
				currentProgress = newProgress;
				_validatedProgress = _currentProgress;
			}

			return true;
		}
#endif

		[SerializeField]
		private int _activationThreshold;

		[SerializeField]
		private IntegerEvent _progressChanged;

		public bool isActive => _currentProgress >= _activationThreshold;

		public int currentProgress
		{
			get => _currentProgress;
			set
			{
				ValidateRecuiredProgress(ref value);
				if (value == _currentProgress)
					return;

				_currentProgress = value;

				_progressChanged?.Invoke(
					value);
			}
		}

		public void ModifyScore(ref float score, CodeHandlerInterface.HadnlingResult.Type handlingResultType)
		{
			if (score < 0 ||
				GameManager.instance.currentCode is NormalCode &&
				handlingResultType == CodeHandlerInterface.HadnlingResult.Type.aborted)
				Reset();
			else
			if (score > 0)
			{
				if (isActive)
					score *= _multiplier;
				currentProgress++;
			}
		}

		public void Reset()
		{
			currentProgress = 0;
		}

		bool ValidateRecuiredProgress(ref int requiredProgress)
		{
			if (requiredProgress < 0)
			{
				requiredProgress = 0;
				return false;
			}

			if (requiredProgress > _activationThreshold)
			{
				requiredProgress = _activationThreshold;
				return false;
			}

			return true;
		}
	}
}