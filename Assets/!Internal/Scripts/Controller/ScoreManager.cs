
using UnityEngine;
using UnityEngine.Events;
using ICD.Engine.Core.CustomAttributes;
using ICD.Engine.Core.Singletons;

using ICD.CashMachine.Controller.Interfaces;
using System;

namespace ICD.CashMachine.Controller
{
	public class ScoreManager : Singleton<ScoreManager>, ScoreManagerInterface
	{
		[SerializeField]
		[RequireInterface(typeof(ScoreModifierInterface))]
		private UnityEngine.Object[] _scoreModifiers;

		[SerializeField]
		private float _totalScore;
		public float totalScore => _totalScore;

		[SerializeField]
		private FloatEvent
			_scoreAdded,
			_scoreChanged;

		public event EventHandler<float>
			ScoreAdded,
			TotalScoreChanged;

		public override bool dontDestroyOnLoad => false;

		public ScoreManager()
		{
			ScoreAdded += HandleScoreAddingEvent;
			TotalScoreChanged += HandleScoreChangingEvent;
		}

		[ContextMenu("Reset Score")]
		public void ResetScore()
		{
			_totalScore = 0;
			TotalScoreChanged?.Invoke(
				this,
				_totalScore);

			foreach (ScoreModifierInterface modifier in _scoreModifiers)
				modifier?.Reset();
		}

		void ScoreManagerInterface.AddScore(float addingScore, CodeHandlerInterface.HadnlingResult.Type handlingResultType, bool useModifiers)
		{
			if (useModifiers)
				foreach (ScoreModifierInterface modifier in _scoreModifiers)
					modifier?.ModifyScore(
						ref addingScore,
						handlingResultType);

			_totalScore += addingScore;
			if (_totalScore < 0)
				_totalScore = 0;

			ScoreAdded?.Invoke(
				this,
				addingScore);
			TotalScoreChanged?.Invoke(
				this,
				_totalScore);
		}

		void HandleScoreAddingEvent(object sender, float addedScore) =>
			_scoreAdded?.Invoke(
				addedScore);

		void HandleScoreChangingEvent(object sender, float totalScore) =>
			_scoreChanged?.Invoke(
				totalScore);
	}
}