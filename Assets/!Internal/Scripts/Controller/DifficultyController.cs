
using System;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using ICD.Engine.Core.Singletons;
using ICD.Engine.Core.CustomAttributes;
using ICD.Engine.Core.Utility;
using CurveJSON = ICD.Engine.Core.Utility.AnimationCurveHelper.Curve;

using ICD.CashMachine.Model;
using ICD.CashMachine.Model.Interfaces;

namespace ICD.CashMachine.Controller
{
	[ExecuteAlways]
	public class DifficultyController : Singleton<DifficultyController>, DifficultyControllerInterface
	{
		[SerializeField]
		private SerializedCurve _difficultyByNormalizedGameTime;

		[SerializeField]
		[ReadOnly]
		private float _currentDifficulty;

		[SerializeField]
		[RequireInterface(typeof(DifficultyObjectInterface))]
		[Required]
		private UnityEngine.Object[] _difficultyObjects;
		public float currentDifficulty => _currentDifficulty;

		public override bool dontDestroyOnLoad => false;

		void UpdateDifficulty(float difficulty)
		{
			_currentDifficulty =
				_difficultyByNormalizedGameTime ?
					_difficultyByNormalizedGameTime.value.Evaluate(
						GameTimer.instance.normalizedGameTime) :
					0;

			foreach (DifficultyObjectInterface difficultObject in _difficultyObjects)
				if (difficultObject != null)
					difficultObject.UpdateDifficulty(
						_currentDifficulty);
		}

		void Update()
		{
			UpdateDifficulty(
				currentDifficulty);
		}

		void DifficultyControllerInterface.Start() { }

		void DifficultyControllerInterface.Stop() { }
	}
}
