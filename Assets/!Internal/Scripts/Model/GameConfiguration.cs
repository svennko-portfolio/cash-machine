
using System.IO;

using UnityEngine;
using Sirenix.OdinInspector;
using ICD.Engine.Core.CustomAttributes;

using ICD.CashMachine.Model.Interfaces;

namespace ICD.CashMachine.Model
{
	[CreateAssetMenu(menuName = "ICD/Cash Machine/Game Configuration", fileName = "Game Configuration")]
	public sealed class GameConfiguration : ScriptableObject
	{
		[SerializeField]
		private string _configurationDirectoryPath;

		[SerializeField]
		[Required]
		[RequireInterface(typeof(SerializedObjectInterface))]
		private UnityEngine.Object[] _serializedObjects;

		private static GameConfiguration _instance;
		internal static GameConfiguration instance
		{
			get
			{
				if (!_instance)
					_instance =
						Resources.Load<GameConfiguration>(
							"Game Configuration");

				return _instance;
			}
		}


		[ContextMenu("Serialize")]
		public void Serialize()
		{
			foreach (SerializedObjectInterface serializedObject in _serializedObjects)
				Serialize(
					serializedObject);
		}

		[ContextMenu("Deserialize")]
		public void Deserialize()
		{
			foreach (SerializedObjectInterface serializedObject in _serializedObjects)
				Deserialize(
					serializedObject);
		}

		public static void Serialize(SerializedObjectInterface serializedObject)
		{
			if (!DefineFilePath(
					serializedObject,
					out string filePath))
				return;

			switch (serializedObject)
			{
				case JSONSerializedObjectInterface JSONObject:
					SerializeJSON(
						JSONObject,
						filePath);
					break;
			}
		}

		public static void Deserialize(SerializedObjectInterface serializedObject)
		{
			if (!DefineFilePath(
					serializedObject,
					out string filePath))
				return;

			switch (serializedObject)
			{
				case JSONSerializedObjectInterface JSONObject:
					DeserializeJSON(
						JSONObject,
						filePath);
					break;
			}
		}

		static bool DefineFilePath(SerializedObjectInterface serializedObject, out string filePath)
		{
			filePath = string.Empty;

			var unityObject = serializedObject as UnityEngine.Object;
			if (!unityObject)
				return false;

			var directory =
				Directory.Exists(instance._configurationDirectoryPath) ?
					new DirectoryInfo(
						instance._configurationDirectoryPath) :
					Directory.CreateDirectory(
						instance._configurationDirectoryPath);

			filePath =
				Path.Combine(
					directory.FullName,
					$"{unityObject}.conf");
			return true;
		}

		static void SerializeJSON(JSONSerializedObjectInterface serializedObject, string filePath)
		{
			if (serializedObject == null)
				return;

			if (serializedObject.SerializeSelf(out string JSON))
				File.WriteAllText(
					filePath,
					JSON);
		}

		static void DeserializeJSON(JSONSerializedObjectInterface serializedObject, string filePath)
		{
			if (serializedObject == null ||
				!File.Exists(filePath))
				return;

			serializedObject.DeserializeSelf(
				File.ReadAllText(
					filePath));
		}

		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		static void Initialize() =>
			instance.Deserialize();
	}
}
