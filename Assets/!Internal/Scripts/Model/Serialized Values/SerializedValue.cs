
using System;

using UnityEngine;
using ICD.Engine.Core.Utility;

using ICD.CashMachine.Model.Interfaces;

namespace ICD.CashMachine.Model
{
	[CreateAssetMenu(menuName = "ICD/Cash Machine/Serialized Values", fileName = "Serialized Value")]
	public abstract class SerializedValue<SerializedValueType> : ScriptableObject, JSONSerializedObjectInterface
	{
		[SerializeField]
		private SerializedValueType _value;
		public SerializedValueType value => _value;

		public static implicit operator SerializedValueType(SerializedValue<SerializedValueType> curveObject) =>
			curveObject._value;

		#region Serialized Object

		[Serializable]
		struct JSONRepresentation
		{
			public SerializedValueType value;
		}

		protected virtual bool SerializeValue(out string serializedData, SerializedValueType value)
		{
			serializedData = string.Empty;
			try
			{
				serializedData =
					JsonUtility.ToJson(
						new JSONRepresentation
						{
							value = _value
						},
						true);
				return true;
			}
			catch (Exception exception)
			{
				Debug.LogException(
					exception);
				return false;
			}
		}

		protected virtual bool DeserializeValue(string serializedData, out SerializedValueType value)
		{
			value = default;
			try
			{
				value =
					JsonUtility.FromJson<JSONRepresentation>(
						serializedData).value;
				return true;
			}
			catch (Exception exception)
			{
				Debug.LogException(
					exception);
				return false;
			}
		}

		bool SerializedObjectInterface.SerializeSelf(out object serializedData) =>
			(this as JSONSerializedObjectInterface).SerializeSelf(
				out serializedData);

		bool SerializedObjectInterface.DeserializeSelf(object serializedData) =>
			(this as JSONSerializedObjectInterface).DeserializeSelf(
				(string)serializedData);

		bool SerializedObjectInterface<string>.DeserializeSelf(string serializedData) =>
			DeserializeValue(
				serializedData,
				out _value);

		bool SerializedObjectInterface<string>.SerializeSelf(out string serializedData) =>
			SerializeValue(
				out serializedData,
				_value);
		#endregion
	}
}
