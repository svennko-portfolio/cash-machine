
using UnityEngine;

namespace ICD.CashMachine.Model
{
	[CreateAssetMenu(menuName = "ICD/Cash Machine/Serialized Values/Float", fileName = "Serialized Float Value")]
	public sealed class SerializedFloatValue : SerializedValue<float> { }
}
