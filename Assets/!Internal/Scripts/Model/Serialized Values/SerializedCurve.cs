
using System;

using UnityEngine;
using ICD.Engine.Core.Utility;

using ICD.CashMachine.Model.Interfaces;

namespace ICD.CashMachine.Model
{
	[CreateAssetMenu(menuName = "ICD/Cash Machine/Serialized Values/Curve", fileName = "Serialized Curve")]
	public sealed class SerializedCurve : SerializedValue<AnimationCurve>
	{
		protected sealed override bool SerializeValue(out string serializedData, AnimationCurve value) =>
			value.ToJSON(
				out serializedData);

		protected sealed override bool DeserializeValue(string serializedData, out AnimationCurve value) =>
			AnimationCurveHelper.FromJSON(
				serializedData,
				out value);
	}
}
