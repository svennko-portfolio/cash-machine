
using System;

using UnityEngine;

using ICD.CashMachine.Model;

namespace ICD.CashMachine.Utility
{
	[CreateAssetMenu(menuName = "ICD/Cash Machine/Parameters Weights/Code Generation/Normal Code Weight", fileName = "Normal Code Weight")]
	public sealed class NormalCodeWeightByDifficulty : CodeTypeWeightByDifficulty<NormalCode>
	{ }
}
