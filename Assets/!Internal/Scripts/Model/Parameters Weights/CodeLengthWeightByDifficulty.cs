
using UnityEngine;

namespace ICD.CashMachine.Utility
{
	[CreateAssetMenu(menuName = "ICD/Cash Machine/Parameters Weights/Code Length Weight", fileName = "Code Length Weight")]
	public class CodeLengthWeightByDifficulty : ParameterWeightByDifficulty<ushort> { }
}
