
using System;

using UnityEngine;

using ICD.CashMachine.Model;

namespace ICD.CashMachine.Utility
{
	public abstract class CodeTypeWeightByDifficulty : ParameterWeightByDifficulty<Type>
	{
		protected abstract Type codeType { get; }
		public sealed override Type parameter => codeType;
	}
	public abstract class CodeTypeWeightByDifficulty<CodeType> : CodeTypeWeightByDifficulty
		where CodeType : Code
	{
		protected sealed override Type codeType => typeof(CodeType);
	}
}
