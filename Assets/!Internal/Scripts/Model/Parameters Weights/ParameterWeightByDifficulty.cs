
using System;
using System.Collections.Generic;
using System.IO;

using UnityEngine;
using ICD.Engine.Core.Utility;
using CurveJSON = ICD.Engine.Core.Utility.AnimationCurveHelper.Curve;

using ICD.CashMachine.Model.Interfaces;

namespace ICD.CashMachine.Utility
{
	public abstract class ParameterWeightByDifficulty<ParameterType> : ScriptableObject, JSONSerializedObjectInterface
	{
		[SerializeField]
		private ParameterType _parameter;
		public virtual ParameterType parameter => _parameter;

		[SerializeField]
		private AnimationCurve _weightByDifficulty = AnimationCurve.Constant(0, 1, 1);
		public float GetWeightByDifficulty(float difficulty)
		{
			var weight = _weightByDifficulty.Evaluate(difficulty);
			return
				weight > 0 ?
					weight :
					0;
		}

		#region Serialized Object

		[Serializable]
		struct ParameterWeightByDifficultyJSON
		{
			public ParameterType parameter;
			public CurveJSON weightByDifficulty;
		}
		bool SerializedObjectInterface.SerializeSelf(out object serializedData) =>
			(this as SerializedObjectInterface<string>).SerializeSelf(
				out serializedData);

		bool SerializedObjectInterface.DeserializeSelf(object serializedData) =>
			(this as SerializedObjectInterface<string>).DeserializeSelf(
				(string)serializedData);

		bool SerializedObjectInterface<string>.SerializeSelf(out string serializedData)
		{
			serializedData =
				JsonUtility.ToJson(
					new ParameterWeightByDifficultyJSON()
					{
						parameter = _parameter,
						weightByDifficulty =
							CurveJSON.FromCurve(
								_weightByDifficulty)
					},
					true);
			return true;
		}

		bool SerializedObjectInterface<string>.DeserializeSelf(string serializedData)
		{
			try
			{
				var representation =
					JsonUtility.FromJson<ParameterWeightByDifficultyJSON>(
						serializedData);

				_parameter = representation.parameter;
				_weightByDifficulty = representation.weightByDifficulty.curve;

				return true;
			}
			catch (Exception exception)
			{
				Debug.LogException(
					exception);
				return false;
			}
		}
		#endregion
	}

	public class ParameterValueComparer<ParameterType> : IEqualityComparer<ParameterWeightByDifficulty<ParameterType>>
	{
		public bool Equals(ParameterWeightByDifficulty<ParameterType> parameterWeight1, ParameterWeightByDifficulty<ParameterType> parameterWeigh2) =>
			parameterWeight1 &&
			parameterWeight1.parameter.Equals(
				parameterWeigh2.parameter);

		public int GetHashCode(ParameterWeightByDifficulty<ParameterType> parameterWeight) =>
			parameterWeight ?
				parameterWeight.parameter.GetHashCode() :
				0;
	}
}
