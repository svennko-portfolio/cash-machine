
using System;
using System.Collections.Generic;

namespace ICD.CashMachine.Utility
{
    public static class RandomParametersHelper
    {
		public static ParameterType SelectRandomByWeights<ParameterType>(this IReadOnlyDictionary<ParameterType, float> probabilityWeights)
		{
			if (probabilityWeights == null)
				throw
					new ArgumentNullException(
						nameof(probabilityWeights));


			var totalWeight = 0f;
			foreach (var weight in probabilityWeights.Values)
				if (weight > 0)
					totalWeight += weight;

			if (totalWeight == 0)
				throw
					new ArgumentException(
						"Total weight must be greater than 0",
						nameof(probabilityWeights));

			var randomValue =
				UnityEngine.Random.Range(
					0,
					totalWeight);

			totalWeight = 0f;
			foreach (var weight in probabilityWeights)
				if (weight.Value > 0)
				{
					if (randomValue >= totalWeight && randomValue < totalWeight + weight.Value)
						return weight.Key;
					totalWeight += weight.Value;
				}
			return default;
		}
	}
}
