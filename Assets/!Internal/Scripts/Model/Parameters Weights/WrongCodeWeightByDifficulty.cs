
using System;

using UnityEngine;

using ICD.CashMachine.Model;

namespace ICD.CashMachine.Utility
{
	[CreateAssetMenu(menuName = "ICD/Cash Machine/Parameters Weights/Code Generation/Wrong Code Weight", fileName = "Wrong Code Weight")]
	public sealed class WrongCodeWeightByDifficulty : CodeTypeWeightByDifficulty<WrongCode>
	{ }
}
