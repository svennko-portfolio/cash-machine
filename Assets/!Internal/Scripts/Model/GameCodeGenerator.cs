
using System;
using System.Collections.Generic;

using UnityEngine;
using Sirenix.OdinInspector;
using ICD.Engine.Core.Utility;
using CurveJSON = ICD.Engine.Core.Utility.AnimationCurveHelper.Curve;

using ICD.CashMachine.Utility;
using ICD.CashMachine.Model.Interfaces;

namespace ICD.CashMachine.Model
{
	[CreateAssetMenu(menuName = "ICD/Cash Machine/Code Generation/Game Code Generator", fileName = "Game Code Generator")]
	public class GameCodeGenerator : ScriptableObject, CodeGeneratorInterface, DifficultyObjectInterface, JSONSerializedObjectInterface
	{
		[SerializeField]
		[ValidateInput("SetDifficultyFromInspector")]
		private float _difficulty = 1;
#if UNITY_EDITOR
		bool SetDifficultyFromInspector(float newDifficulty)
		{
			(this as DifficultyObjectInterface).UpdateDifficulty(
				newDifficulty);
			return true;
		}
#endif
		public float difficulty => _difficulty;

		[SerializeField]
		[Required]
		private CodeTypeWeightByDifficulty[] _codeTypeWeightsByDifficulty;

		[SerializeField]
		[Required]
		private CodeLengthWeightByDifficulty[] _codeLengthWeightsByDifficulty;

		[SerializeField]
		private AnimationCurve _expirationDurationByDifficulty;

		[SerializeField]
		[Required]
		private SymbolsSet _symbolsSet;

		private Dictionary<Type, float> _currnetCodeTypeWeights;
		private Dictionary<ushort, float> _currentCodeLengthWeights;


		public CodeType GenerateCode<CodeType>(ushort length, float exirationDuration = Code.DEFAULT_EXPIRATION_DURATION, bool autoActivate = false)
			where CodeType : Code, new()
		{
			var newCode =
				new CodeType()
				{
					value = _symbolsSet.GetRandomString(length),
					expirationDuration = exirationDuration
				};

			if (autoActivate)
				newCode.Activate();

			return newCode;
		}

		public Code GenerateRandomCode(float exirationDuration = Code.DEFAULT_EXPIRATION_DURATION, bool autoActivate = false)
		{
			if (_currnetCodeTypeWeights == null)
				SetParameterWeights(
					_codeTypeWeightsByDifficulty,
					ref _currnetCodeTypeWeights,
					_difficulty);
			var codeType = _currnetCodeTypeWeights.SelectRandomByWeights();

			if (_currentCodeLengthWeights == null)
				SetParameterWeights(
				_codeLengthWeightsByDifficulty,
				ref _currentCodeLengthWeights,
				_difficulty);
			var codeLength = _currentCodeLengthWeights.SelectRandomByWeights();

			var expirationDuration =
				_expirationDurationByDifficulty.Evaluate(
					_difficulty);

			Code code = null;

			if (codeType == typeof(NormalCode))
				code =
					GenerateCode<NormalCode>(
						codeLength,
						expirationDuration,
						autoActivate);
			else
			if (codeType == typeof(WrongCode))
				code =
					GenerateCode<WrongCode>(
						codeLength,
						expirationDuration,
						autoActivate);

			Debug.Log($"Generated Code: {code}");
			return code;
		}

		void DifficultyObjectInterface.UpdateDifficulty(float newDifficulty)
		{
			SetParameterWeights(
				_codeTypeWeightsByDifficulty,
				ref _currnetCodeTypeWeights,
				newDifficulty);

			SetParameterWeights(
				_codeLengthWeightsByDifficulty,
				ref _currentCodeLengthWeights,
				newDifficulty);

			_difficulty = newDifficulty;
		}

		void SetParameterWeights<ParameterType>(IReadOnlyCollection<ParameterWeightByDifficulty<ParameterType>> weightsByDifficulty, ref Dictionary<ParameterType, float> currentWeights, float difficulty)
		{
			if (currentWeights == null)
				currentWeights = new Dictionary<ParameterType, float>(weightsByDifficulty.Count);
			else
				currentWeights.Clear();

			foreach (var weightByDifficulty in weightsByDifficulty)
				if (!currentWeights.ContainsKey(weightByDifficulty.parameter))
				{
					var weight =
						weightByDifficulty.GetWeightByDifficulty(
							difficulty);
					if (weight > 0)
						currentWeights.Add(
							weightByDifficulty.parameter,
							weight);
				}
		}

		void Awake() =>
			(this as DifficultyObjectInterface).UpdateDifficulty(
				_difficulty);

		#region Serialized Object
		[Serializable]
		struct GameCodeGeneratorJSON
		{
			public CurveJSON expirationDurationByDifficulty;
		}

		bool SerializedObjectInterface<string>.SerializeSelf(out string serializedData)
		{
			serializedData =
				JsonUtility.ToJson(
					new GameCodeGeneratorJSON()
					{
						expirationDurationByDifficulty = CurveJSON.FromCurve(
							_expirationDurationByDifficulty)
					},
				true);
			return true;
		}

		bool SerializedObjectInterface<string>.DeserializeSelf(string serializedData)
		{
			try
			{
				_expirationDurationByDifficulty =
					JsonUtility.FromJson<GameCodeGeneratorJSON>(
						serializedData).expirationDurationByDifficulty.curve;
				return true;
			}
			catch(Exception exception)
			{
				Debug.LogException(
					exception);
				return false;
			}
		}

		bool SerializedObjectInterface.SerializeSelf(out object serializedData) =>
			(this as SerializedObjectInterface<string>).SerializeSelf(
				out serializedData);

		bool SerializedObjectInterface.DeserializeSelf(object serializedData) =>
			(this as SerializedObjectInterface<string>).DeserializeSelf(
				(string)serializedData);
		#endregion
	}
}
