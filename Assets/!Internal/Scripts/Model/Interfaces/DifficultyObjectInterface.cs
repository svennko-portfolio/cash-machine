
namespace ICD.CashMachine.Model.Interfaces
{
    public interface DifficultyObjectInterface
    {
        public float difficulty { get; }
        public void UpdateDifficulty(float newDifficulty);
    }
}
