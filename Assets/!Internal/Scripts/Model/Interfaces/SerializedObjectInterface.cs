
using System.IO;

namespace ICD.CashMachine.Model.Interfaces
{
	public interface SerializedObjectInterface
	{
		public bool SerializeSelf(out object serializedData);
		public bool DeserializeSelf(object serializedData);
	}

	public interface SerializedObjectInterface<DataType> : SerializedObjectInterface
	{
		public bool SerializeSelf(out DataType serializedData);
		public bool DeserializeSelf(DataType serializedData);
	}

	public interface JSONSerializedObjectInterface : SerializedObjectInterface<string> { }
}
