
namespace ICD.CashMachine.Model.Interfaces
{
	public interface CodeGeneratorInterface
	{
		public CodeType GenerateCode<CodeType>(ushort length, float exirationDuration = Code.DEFAULT_EXPIRATION_DURATION, bool autoActivate = false)
			where CodeType : Code, new();
		public Code GenerateRandomCode(float exirationDuration = Code.DEFAULT_EXPIRATION_DURATION, bool autoActivate = false);
	}
}
