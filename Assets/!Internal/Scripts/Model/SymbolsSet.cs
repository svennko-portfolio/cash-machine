
using System;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using Sirenix.OdinInspector;

namespace ICD.CashMachine.Model
{
    [CreateAssetMenu(menuName = "ICD/Cash Machine/Code Generation/Symbols Set", fileName = "Symbols Set")]
    public class SymbolsSet : ScriptableObject
    {
        [SerializeField]
        [Required]
        private char[] _symbols = new char[0];

        public IReadOnlyList<char> symbols => _symbols;

        public string GetRandomString(ushort length = 1)
		{
			if (symbols == null || symbols.Count == 0)
				throw
					new InvalidOperationException(
						"Symbols set is empty");

			var builder = new StringBuilder(length);
			for (int s = 0; s < length; s++)
				builder.Append(
					symbols[
						UnityEngine.Random.Range(
							0,
							symbols.Count)]);
			return builder.ToString();
		}
    }
}
