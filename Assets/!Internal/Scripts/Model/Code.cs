
using System;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;

namespace ICD.CashMachine.Model
{
	[Serializable]
	public abstract class Code
	{
		public const float DEFAULT_EXPIRATION_DURATION = 5;

		[SerializeField]
		private string _value;

		public event EventHandler Activated;
		public event EventHandler<ExpirationEventArguments> Expired;

		[Serializable]
		public class ExpirationEventArguments : EventArgs
		{
			public enum Reason
			{
				unknown,
				timeout,
				abort
			}

			public Reason reason { get; }
			public ExpirationEventArguments(Reason reason = Reason.timeout) =>
				this.reason = reason;
		}

		public string value
		{
			get => _value;
			internal set => _value = value;
		}
		public bool isActive => _invokationCancellation != null;
		public bool isExpired { get; private set; }

		public float expirationDuration { get; internal set; } = DEFAULT_EXPIRATION_DURATION;
		public float activationTime { get; private set; }
		public float normalizedActivationTime => activationTime / expirationDuration;
		public float timeToExpiration => expirationDuration - activationTime;
		public float normalizedTimeToExpiration => 1 - normalizedActivationTime;

		private CancellationTokenSource _invokationCancellation;
		protected CancellationToken _invokationCancellationToken =>
			_invokationCancellation != null ?
				_invokationCancellation.Token :
				CancellationToken.None;
		internal async Task Activate(CancellationToken cancellationToken = default)
		{
			_invokationCancellation?.Cancel();
			_invokationCancellation = new CancellationTokenSource();
			cancellationToken =
				CancellationTokenSource.CreateLinkedTokenSource(
					cancellationToken,
					_invokationCancellation.Token).Token;

			isExpired = false;
			this.expirationDuration = expirationDuration;
			activationTime = 0;
			Activated?.Invoke(
				this,
				EventArgs.Empty);

			ExpirationEventArguments.Reason expirationReason = ExpirationEventArguments.Reason.timeout;

			for (
					float startTime = Time.time;

					activationTime < expirationDuration;

					activationTime = Time.time - startTime
				)
			{
				if (cancellationToken.IsCancellationRequested)
				{
					expirationReason = ExpirationEventArguments.Reason.abort;
					return;
				}

				await Task.Yield();
			}

			_invokationCancellation?.Dispose();
			_invokationCancellation = null;

			isExpired = true;

			Expired?.Invoke(
				this,
				new ExpirationEventArguments(
					expirationReason));
		}

		internal void Abort()
		{
			_invokationCancellation?.Cancel();
			_invokationCancellation = null;
		}

		public bool IsMatching(string comparingString, out bool fullMatching)
		{
			fullMatching = false;

			if (string.IsNullOrEmpty(comparingString))
				return true;

			if (comparingString.Length < _value.Length)
				return
					_value.StartsWith(
						comparingString);
			else
			{
				comparingString = comparingString.Substring(0, _value.Length);
				fullMatching = comparingString == _value;
				return fullMatching;
			}
		}

		public override string ToString() =>
			$"Code type of {GetType().Name}: {value}";

		public static implicit operator bool(Code code) =>
			code != null;
	}

	[Serializable]
	public class NormalCode : Code { }

	[Serializable]
	public class WrongCode : Code { }
}
