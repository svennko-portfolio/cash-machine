
using UnityEngine;
using TMPro;
using Sirenix.OdinInspector;

using ICD.CashMachine.Controller;

namespace ICD.CashMachine.View
{
	public class InputController : MonoBehaviour
	{
		[SerializeField]
		[SceneObjectsOnly]
		[Required]
		private TMP_InputField _inputField;

		[SerializeField]
		[SceneObjectsOnly]
		private CanvasGroup _keyboardCanvasGroup;

		[SerializeField]
		[ValidateInput("SetInteractableFromInpector")]
		private bool _interactable;
#if UNITY_EDITOR
		private bool _validatedInteractableState;
		bool SetInteractableFromInpector(bool newState)
		{
			if (newState != _validatedInteractableState)
			{
				_interactable = _validatedInteractableState;
				interactable = newState;
				_validatedInteractableState = _interactable;
			}

			return true;
		}
#endif
		public bool interactable
		{
			get => _interactable;
			set
			{
				if (value == _interactable)
					return;

				_inputField.interactable = value;
				if (value)
					RestoreSelection();
				if (_keyboardCanvasGroup)
					_keyboardCanvasGroup.interactable = value;

				_interactable = value;
			}
		}

		public void AddSymbols(string newSymbols)
		{
			_inputField.text += newSymbols;
			RestoreSelection();
		}

		public void RemoveSymbols(int count)
		{
			if (count > _inputField.text.Length)
				count = _inputField.text.Length;
			if (count <= 0)
				return;

			_inputField.text =
				_inputField.text.Substring(
					0,
					_inputField.text.Length - count);
			RestoreSelection();
		}

		public void RemoveAll()
		{
			_inputField.text = string.Empty;
			RestoreSelection();
		}

		void RestoreSelection()
		{
			_inputField.Select();
			_inputField.caretPosition = _inputField.text.Length;
		}
	}
}
