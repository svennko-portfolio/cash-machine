
using System;

using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;

using ICD.CashMachine.Controller;

namespace ICD.CashMachine.View
{
	[DisallowMultipleComponent]
	[ExecuteAlways]
	public class GameTimeUI : MonoBehaviour
	{
		[SerializeField]
		private bool _showRemainingTime;

		[SerializeField]
		[SceneObjectsOnly]
		[Required]
		private Slider[] _timeSliders;

		[SerializeField]
		[SceneObjectsOnly]
		[Required]
		private TMP_Text[] _gameTimeFields;

		[SerializeField]
		[ShowIf("_gameTimeFields")]
		private string _gameTimeFormat;


		[SerializeField]
		[SceneObjectsOnly]
		[Required]
		private TMP_Text[] _timeToExpirationFields;

		void UpdateSlider(float timeValue, float duration)
		{
			foreach (var slider in _timeSliders)
				if (slider)
				{
					slider.minValue = 0;
					slider.maxValue = duration;
					slider.value = timeValue;
				}
		}

		void UpdateGameTimeFields(float timeValue)
		{
			var text =
				TimeSpan.FromSeconds(
					timeValue).
				ToString(
					_gameTimeFormat);

			foreach (var field in _gameTimeFields)
				if (field)
					field.text = text;
		}

		void UpdateTimeToExpirationAnswerFields()
		{
			var code = GameManager.instance.currentCode;
			if (!code)
				return;

			var text = ((int)(code.timeToExpiration + .995f)).ToString();

			foreach (var field in _timeToExpirationFields)
				if (field)
					field.text = text;
		}

		void Update()
		{
			var duration = GameTimer.instance.gameDuration;
			var timeValue = GameTimer.instance.currentGameTime;
			if (_showRemainingTime)
				timeValue = duration - timeValue;

			UpdateSlider(
				timeValue,
				duration);
			UpdateGameTimeFields(
				timeValue);
			UpdateTimeToExpirationAnswerFields();
		}
	}
}
