
using UnityEngine;
using TMPro;
using Sirenix.OdinInspector;

namespace ICD.CashMachine.View
{
    [RequireComponent(typeof(CodePush))]
    public class RandomLablelSetter : MonoBehaviour
    {
        [SerializeField]
        [ChildGameObjectsOnly]
        [Required]
        private TMP_Text _lableField;

        [SerializeField]
        [Required]
        private string[] _variants = new string[0];

		void Awake()
		{
            if (_lableField)
                _lableField.text =
                    _variants[
                        Random.Range(
                            0,
                            _variants.Length)];
		}
	}
}
