
using System;

using UnityEngine;
using ICD.Engine.Core.CustomAttributes;

using ICD.CashMachine.Model;
using ICD.CashMachine.Model.Interfaces;

namespace ICD.CashMachine.View
{
	public class IdlePushSpawner : PushSpawner
	{
		[SerializeField]
		private bool _autoStartOnEnable = true;

		[SerializeField]
		[RequireInterface(typeof(CodeGeneratorInterface))]
		private UnityEngine.Object _generator;
		private CodeGeneratorInterface generator => _generator as CodeGeneratorInterface;

		[SerializeField]
		[Min(1)]
		private float
			_expirationDuration = 5,
			_normalizedShowingDuration = 1;

		private Code _currentCode;

		[ContextMenu("Spawn New")]
		public void SpawnNew()
		{
			if (_currentCode)
				_currentCode.Expired -= HandleCodeExpiration;

			_currentCode =
				generator.GenerateRandomCode(
					_expirationDuration,
					true);

			_currentCode.Expired += HandleCodeExpiration;

			SpawnNewCode(
				_currentCode,
				_normalizedShowingDuration);
		}

		void HandleCodeExpiration(object sender, Code.ExpirationEventArguments eventArguments)
		{
			var code = sender as Code;
			if (code)
				code.Expired -= HandleCodeExpiration;

			SpawnNew();
		}

		void OnEnable()
		{
			if (_autoStartOnEnable)
				SpawnNew();
		}

		void OnDestroy()
		{
			if (_currentCode)
				_currentCode.Expired -= HandleCodeExpiration;
		}
	}
}
