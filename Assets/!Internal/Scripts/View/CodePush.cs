
using System;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.Events;
using TMPro;
using Sirenix.OdinInspector;
using ICD.Engine.Core.Utility;

using ICD.CashMachine.Model;

namespace ICD.CashMachine.View
{
	[DisallowMultipleComponent]
	public class CodePush : MonoBehaviour
	{
		[SerializeField]
		private Code _code;
		public Code code => _code;

		[SerializeField]
		[ChildGameObjectsOnly]
		[Required]
		private TMP_Text _codeField;

		[SerializeField]
		private BooleanEvent _displayingStateChanged;

		[SerializeField]
		private UnityEvent
			_fullyDeployed,
			_expired;

		public event EventHandler FullyDeployed;
		public event EventHandler<bool> DisplayingStateChanged;

		public bool isDisplaying => _displayCancellation != null;

		private CancellationTokenSource
			_destroyCancellation = new CancellationTokenSource(),
			_displayCancellation;

		public CodePush()
		{
			DisplayingStateChanged += HandleDisplayingStateEvent;
			FullyDeployed += HandleFullDeploymentEvent;
		}


		public async void Display(Code code, float normalizedDisplayDuration = 1, CancellationToken cancellationToken = default)
		{
			if (isDisplaying)
				throw
					new InvalidOperationException(
						"Push alredy displaying");

			_code = code;
			_codeField.text = code.value;

			if (code.isExpired)
			{
				HandleExpirationEvent(
					this,
					new Code.ExpirationEventArguments(
						Code.ExpirationEventArguments.Reason.unknown));
				return;
			}

			code.Expired += HandleExpirationEvent;

			_displayCancellation = new CancellationTokenSource();
			cancellationToken =
				CancellationTokenSource.CreateLinkedTokenSource(
					cancellationToken,
					_displayCancellation.Token,
					_destroyCancellation.Token).Token;

			DisplayingStateChanged?.Invoke(
				this,
				true);

			while (!code.isActive)
			{
				if (cancellationToken.IsCancellationRequested)
					break;
				await Task.Yield();
			}

			for (
					float startTime = Time.time,
					ellapsedTime = 0;

					ellapsedTime < code.expirationDuration * normalizedDisplayDuration;

					ellapsedTime = Time.time - startTime
				)
			{
				if (cancellationToken.IsCancellationRequested)
					break;

				await Task.Yield();
			}

			_displayCancellation?.Dispose();
			_displayCancellation = null;

			DisplayingStateChanged?.Invoke(
				this,
				false);
		}

		[ContextMenu("Invoke Self Destroying")]
		internal void InvokeSelfDestroying() =>
			_displayCancellation?.Cancel();

		public void SetFullyDeployed() =>
			FullyDeployed?.Invoke(
				this,
				EventArgs.Empty);

		void HandleFullDeploymentEvent(object sender, EventArgs eventArguments) =>
			_fullyDeployed?.Invoke();

		void HandleDisplayingStateEvent(object sender, bool displayState) =>
			_displayingStateChanged?.Invoke(
				displayState);

		void HandleExpirationEvent(object sender, Code.ExpirationEventArguments eventArguments)
		{
			InvokeSelfDestroying();
			_code.Expired -= HandleExpirationEvent;
			_expired?.Invoke();
		}

		public void DestroySelf() =>
			gameObject.DestroyByApplicationPlayingState();

		void OnDestroy() =>
			_destroyCancellation?.Cancel();
	}
}
