
using UnityEngine;
using TMPro;
using Sirenix.OdinInspector;

namespace ICD.CashMachine
{
	[DisallowMultipleComponent]
	public class Reward : MonoBehaviour
	{
		[SerializeField]
		[ChildGameObjectsOnly]
		private TMP_Text _valueField;

		[SerializeField]
		private string _valueFormat = "#,#";

		[SerializeField]
		[ValidateInput("SetValueFromInspector")]
		private float _value;
#if UNITY_EDITOR
		private float _validatedValue;
		bool SetValueFromInspector(float newValue)
		{
			if (newValue != _validatedValue)
			{
				_value = _validatedValue;
				value = newValue;
				_validatedValue = _value;
			}

			return true;
		}
#endif
		public float value
		{
			get => _value;
			set
			{
				if (_value == value)
					return;

				if (_valueField)
					_valueField.text =
						Mathf.RoundToInt(value).ToString(
							_valueFormat);

				_value = value;
			}
		}
	}
}
