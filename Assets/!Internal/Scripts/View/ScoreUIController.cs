
using UnityEngine;
using TMPro;
using Sirenix.OdinInspector;
using ICD.Engine.Core.CustomAttributes;
using ICD.Engine.Core.Singletons;

using ICD.CashMachine.Controller.Interfaces;

namespace ICD.CashMachine.View
{
	[ExecuteAlways]
	public class ScoreUIController : MonoBehaviour
	{
		[SerializeField]
		[RequireInterface(typeof(ScoreManagerInterface))]
		[SceneObjectsOnly]
		[Required]
		private Object _scoreManager;
		private ScoreManagerInterface scoreManager => _scoreManager as ScoreManagerInterface;

		[SerializeField]
		[SceneObjectsOnly]
		[Required]
		private TMP_Text[] _scoreFields;

		[SerializeField]
		[ReadOnly]
		private float _currentValue;

		[SerializeField]
		private string _scoreFormat = "#,#";

		[SerializeField]
		[Range(0, 1)]
		private float _lerpIntencity = .15f;

		[SerializeField]
		[SceneObjectsOnly]
		private Transform
			_rewardsAnchor,
			_finesAnchor;

		[SerializeField]
		[AssetsOnly]
		private Reward
			_rewardPrefab,
			_finePrefab;

		void Update()
		{
			_currentValue =
				Mathf.Lerp(
					_currentValue,
					scoreManager.totalScore,
					_lerpIntencity);

			var roundedValue = Mathf.RoundToInt(_currentValue);
			var text =
				roundedValue > 0 ?
					roundedValue.ToString(
						_scoreFormat) :
					"0";

			foreach (var field in _scoreFields)
				if (field)
					field.text = text;
		}

		void HandleScoreAddingEvent(object sender, float addedScore)
		{
			if (addedScore > 0 &&
				_rewardPrefab)
			{
				var reward =
					Instantiate(
						_rewardPrefab,
						_rewardsAnchor);
				reward.value = addedScore;
			}
			else
			if (addedScore < 0 &&
				_finePrefab)
			{
				var fine =
					Instantiate(
						_finePrefab,
						_finesAnchor);
				fine.value = addedScore;
			}
		}

		void Awake()
		{
			scoreManager.ScoreAdded += HandleScoreAddingEvent;
		}
		void OnDestroy()
		{
			scoreManager.ScoreAdded -= HandleScoreAddingEvent;
		}
	}
}
