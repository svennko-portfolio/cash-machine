
using System;

using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using ICD.Engine.Core.CustomAttributes;
using ICD.Engine.Core.Singletons;
using ICD.Engine.Core.Utility;
using CurveJSON = ICD.Engine.Core.Utility.AnimationCurveHelper.Curve;

using ICD.CashMachine.Model;
using ICD.CashMachine.Model.Interfaces;
using ICD.CashMachine.Controller;

namespace ICD.CashMachine.View
{
	[DisallowMultipleComponent]
	public class PushSpawner : Singleton<PushSpawner>, DifficultyObjectInterface
	{
		[SerializeField]
		[SceneObjectsOnly]
		private Transform _pushesAnchor;

		[SerializeField]
		[AssetsOnly]
		[Required]
		private CodePush
			_normalCodePush,
			_wrongCodePush;

		[SerializeField]
		[ReadOnly]
		private CodePush _currentPush;

		[SerializeField]
		private SerializedCurve _normalizedDisplayDurationByDifficulty;

		[SerializeField]
		private float _difficulty;

		[SerializeField]
		private UnityEvent
			_newPushSpawned,
			_newPushDeployed;

		public override bool dontDestroyOnLoad => false;

		public float difficulty => _difficulty;

		public void SpawnNewCode(Code newCode) =>
			SpawnNewCode(
				newCode,
				_normalizedDisplayDurationByDifficulty ?
					_normalizedDisplayDurationByDifficulty.value.Evaluate(
						_difficulty) :
				1);

		public void SpawnNewCode(Code newCode, float normalizedDisplayDuration = 1)
		{
			if (_currentPush)
				_currentPush.InvokeSelfDestroying();

			_currentPush =
				Instantiate(
					GetPrefabByCode(
						newCode),
					_pushesAnchor);

			_currentPush.FullyDeployed += HandleNewPushFullDeploymentEvent;
			_currentPush.Display(
				newCode,
				normalizedDisplayDuration);

			_newPushSpawned?.Invoke();
		}

		void HandleNewPushFullDeploymentEvent(object sender, EventArgs eventArgumnets)
		{
			_currentPush.FullyDeployed -= HandleNewPushFullDeploymentEvent;
			_newPushDeployed?.Invoke();
		}

		CodePush GetPrefabByCode(Code code)
		{
			if (!code)
				throw
					new ArgumentNullException(
						nameof(code));

			switch (code)
			{
				case NormalCode:
				default:
					return _normalCodePush;
				case WrongCode:
					return _wrongCodePush;
			}
		}

		void DifficultyObjectInterface.UpdateDifficulty(float newDifficulty)
		{
			_difficulty = newDifficulty;
		}
	}
}
