
namespace ICD.Engine.Core.Animations
{
	public sealed class AnimatorIntSetter : AnimatorParameterSetter<int>
	{
		protected override int GetParameterFromAnimator() =>
			animator.GetInteger(
				parameterName);
		protected override void SetParameter(int parameterValue) =>
			animator.SetInteger(
				parameterName,
				parameterValue);

		public void AddValue(int value) =>
			parameterValue += value;

		public void SubtractValue(int value) =>
			parameterValue -= value;
	}
}
