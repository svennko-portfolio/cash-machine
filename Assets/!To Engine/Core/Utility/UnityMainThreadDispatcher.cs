
using System;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;

namespace ICD.Engine.Core.Utility
{
	public static class UnityMainThreadDispatcher
	{

		private static SynchronizationContext _mainThreadContext;
		public static SynchronizationContext mainThreadContext => _mainThreadContext;

		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
		static void InitializeSynchronization()
		{
			_mainThreadContext = SynchronizationContext.Current;
		}

		private abstract class Invoker
		{
			public abstract void Invoke();
		}
		public static void Dispatch(Action action) =>
			_mainThreadContext.Send(
				state => action?.Invoke(),
				null);
		public static async Task<ResultType> Dispatch<ResultType>(Func<ResultType> function, bool synchronizeWithCapturedContext = false)
		{
			if (function == null)
				throw new ArgumentException(
					"Delegate is NULL",
					"function");

			ResultType result = default;
			var isCompleted = false;
			_mainThreadContext.Send(
				state =>
				{
					result = function.Invoke();
					isCompleted = true;
				},
				null);
			await Task.Run(
				() =>
				{
					while (!isCompleted) ;
				}).ConfigureAwait(synchronizeWithCapturedContext);

			return result;
		}
	}
}