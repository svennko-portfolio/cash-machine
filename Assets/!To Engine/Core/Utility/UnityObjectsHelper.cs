
using UnityEngine;

namespace ICD.Engine.Core.Utility
{
	public static class UnityObjectsHelper
	{
		public static void DestroyByApplicationPlayingState(this Object unityObject)
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				Object.DestroyImmediate(unityObject);
			else
#endif
				Object.Destroy(unityObject);
		}
		public static void SimpleDestroy(this Object unityObject) =>
				Object.Destroy(unityObject);
		public static void DestroyImmediate(this Object unityObject) =>
				Object.DestroyImmediate(unityObject);
	}
}
