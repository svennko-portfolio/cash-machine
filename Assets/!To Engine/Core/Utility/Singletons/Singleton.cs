
using System;

using UnityEngine;

using ICD.Engine.Core.Utility;

namespace ICD.Engine.Core.Singletons
{
	[DisallowMultipleComponent]
	public abstract class Singleton<FinalType> : Instance
		where FinalType : Singleton<FinalType>
	{
		public enum InstantiateMode
		{
			destroyNew,
			destroyOld
		}
		[SerializeField]
		private InstantiateMode _instantiateMode;
		protected InstantiateMode instantiateMode => _instantiateMode;

		public abstract bool dontDestroyOnLoad { get; }

		private static FinalType _instance;

		private static object _instanceLocker = new object();
		public static FinalType instance
		{
			get
			{
				lock (_instanceLocker)
					if (!_instance)
					{
						var otherInstances = FindObjectsOfType<FinalType>();
						if (otherInstances != null && otherInstances.Length > 0)
						{
							_instance = otherInstances[0];
							for (int i = 1; i < otherInstances.Length; i++)
								DestroyExcessInstance(otherInstances[i]);
						}
						else
						{
							_instance = new GameObject(typeof(FinalType).Name).AddComponent<FinalType>();
							_instance.OnAwaked();
						}
					}
				return _instance;
			}
		}
		public Singleton()
		{
			Awaked += OnAwaked;
			Destroyed += OnDestroyed;
			ApplicationQuited += OnApplicationQuited;

		}
		static void DestroyExcessInstance(FinalType instance)
		{
			if (!instance)
				return;

			UnityEngine.Object destroyTarget = instance;
			if (instance.gameObject != _instance.gameObject)
				destroyTarget = instance.gameObject;

			destroyTarget.DestroyByApplicationPlayingState();
		}
		private void OnAwaked()
		{
			if (dontDestroyOnLoad)
#if UNITY_EDITOR
				if (Application.isPlaying)
#endif
					DontDestroyOnLoad(gameObject);

			lock (_instanceLocker)
			{
				if (!_instance || _instance == this)
				{
					_instance = this as FinalType;
					return;
				}

				switch (instantiateMode)
				{
					case InstantiateMode.destroyNew:
						DestroyExcessInstance(this as FinalType);
						break;
					case InstantiateMode.destroyOld:
						var destroyTarget = _instance;
						_instance = this as FinalType;
						DestroyExcessInstance(destroyTarget);
						break;
				}
			}
		}
		private void OnDestroyed()
		{
			if (_instance == this)
				_instance = null;
		}
		void OnApplicationQuited()
		{
			if (_instance)
				DestroyExcessInstance(_instance);
		}
	}
}
