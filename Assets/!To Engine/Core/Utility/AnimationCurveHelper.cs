
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using UnityEngine;

namespace ICD.Engine.Core.Utility
{
	public static class AnimationCurveHelper
	{
		public struct CurveKey
		{
			public Keyframe frame { get; set; }
			public uint index { get; set; }
			public CurveKey(Keyframe frame, uint index)
			{
				this.frame = frame;
				this.index = index;
			}
		}

		public struct CurveProperties
		{
			private Keyframe _startKey;
			public Keyframe startKey => _startKey;

			private Keyframe _endKey;
			public Keyframe endKey => _endKey;

			public float duration => _endKey.time - _startKey.time;
			public float durationFromZero => _endKey.time;


			private Keyframe _minValueKey;
			public Keyframe minValueKey => _minValueKey;

			private Keyframe _maxValueKey;
			public Keyframe maxValueKey => _maxValueKey;

			public float valueDelta => _maxValueKey.value - _minValueKey.value;

			void SetProperties(Keyframe[] keys)
			{
				if (keys == null || keys.Length == 0)
					return;

				_startKey = keys[0];
				_endKey = keys[keys.Length - 1];
				_minValueKey.value = float.PositiveInfinity;
				_maxValueKey.value = float.NegativeInfinity;

				for (int k = 0; k < keys.Length; k++)
				{
					if (keys[k].value < _minValueKey.value)
						_minValueKey = keys[k];

					if (keys[k].value > _maxValueKey.value)
						_maxValueKey = keys[k];
				}
			}
			public CurveProperties(AnimationCurve curve)
			{
				_startKey =
				_endKey =
				_minValueKey =
				_maxValueKey = new Keyframe();

				if (curve == null)
					return;

				SetProperties(curve.keys);
			}
			public CurveProperties(Keyframe[] keys)
			{
				_startKey =
				_endKey =
				_minValueKey =
				_maxValueKey = new Keyframe();

				SetProperties(keys);
			}
		}

		public static CurveProperties GetProperties(this AnimationCurve curve) =>
			new CurveProperties(curve);

		public static CurveProperties GetCurveProperties(this Keyframe[] keys) =>
			new CurveProperties(keys);

		public static bool ToJSON(this AnimationCurve curve, out string JSONString, bool prettyPrint = true)
		{
			JSONString = string.Empty;

			try
			{
				JSONString =
					JsonUtility.ToJson(
						Curve.FromCurve(curve),
						prettyPrint);

				return true;
			}
			catch (Exception exception)
			{
				Debug.LogException(
					exception);
				return false;
			}
		}

		public static bool ToJSONFile(this AnimationCurve curve, FileInfo file, bool prettyPrint = true)
		{
			if (!file.Directory.Exists)
				Directory.CreateDirectory(
					file.Directory.FullName);

			if (curve.ToJSON(out string JSONString, prettyPrint))
			{
				File.WriteAllText(
					file.FullName,
					JSONString);
				return true;
			}
			else
				return false;
		}

		public static void ToJSONFile(this AnimationCurve curve, string filePath, bool prettyPrint = true) =>
			curve.ToJSONFile(
				new FileInfo(
					filePath),
				prettyPrint);

		public static bool FromJSON(string JSONString, out AnimationCurve curve)
		{
			curve =
				AnimationCurve.Constant(
					0,
					0,
					0);
			try
			{
				curve =
					new AnimationCurve(
						(from key in JsonUtility.FromJson<Curve>(
										JSONString).keys
						 select key.keyFrame).ToArray());

				return true;
			}
			catch (Exception exception)
			{
				Debug.LogException(
					exception);
				return false;
			}
		}
		public static bool FromJSONFile(string filePath, out AnimationCurve curve, bool createIfNotExists = true)
		{
			curve =
				AnimationCurve.Constant(
					0,
					0,
					0);

			if (!File.Exists(filePath))
				if (createIfNotExists)
				{
					curve.ToJSONFile(
						filePath);
					return true;
				}
				else
					return false;

			return
				FromJSON(
					File.ReadAllText(
							filePath),
					out curve);
		}

		public static bool DeserealizeFromJSONFile(FileInfo file, out AnimationCurve curve) =>
			FromJSONFile(
				file.FullName,
				out curve);

		[Serializable]
		public struct Key
		{
			public float time;
			public float value;

			public int tangetMode;
			public float inTanget;
			public float inWeight;

			public string weightedMode;
			public float outTanget;
			public float outWeight;

			public Keyframe keyFrame
			{
				get
				{
					if (!Enum.TryParse<WeightedMode>(weightedMode, true, out WeightedMode mode))
						mode = WeightedMode.None;
					return
						new Keyframe(
							time,
							value,
							inTanget,
							inWeight,
							outTanget,
							outWeight)
						{
							tangentMode = tangetMode,
							weightedMode = mode
						};
				}
			}

			public static Key FromKeyFrame(Keyframe keyFrame) =>
				new Key()
				{
					time = keyFrame.time,
					value = keyFrame.value,

					tangetMode = keyFrame.tangentMode,
					inTanget = keyFrame.inTangent,
					outTanget = keyFrame.outTangent,

					weightedMode = keyFrame.weightedMode.ToString(),
					inWeight = keyFrame.inWeight,
					outWeight = keyFrame.outWeight,
				};
		}

		[Serializable]
		public struct Curve
		{
			public Key[] keys;

			public AnimationCurve curve =>
				new AnimationCurve(
					(from key in keys
					 select key.keyFrame).ToArray());

			public static Curve FromFrames(IEnumerable<Keyframe> frames) =>
				new Curve()
				{
					keys =
						(from frame in frames
						 select Key.FromKeyFrame(
							 frame)).ToArray()
				};

			public static Curve FromCurve(AnimationCurve curve) =>
				FromFrames(
					curve.keys);
		}
	}
}
