
using UnityEngine;

namespace ICD.Engine.Core.Utility
{
    [DisallowMultipleComponent]
    public class AutoDontDestroy : MonoBehaviour
    {
		void Awake()
		{
			if (transform.parent)
				transform.SetParent(
					null);
			DontDestroyOnLoad(gameObject);
		}
	}
}
