
using System;
using System.Collections.Generic;

namespace ICD.Engine.Core.Utility
{
	public class AdditionalDataContainer
	{
		[Serializable]
		public abstract class AdditionalData
		{
			public static implicit operator bool(AdditionalData data) =>
				data != null;
		}

		private Dictionary<Type, AdditionalData> _additionalData = new Dictionary<Type, AdditionalData>();

		public bool SetData<DataType>(DataType data, bool overwrite = false)
			where DataType : AdditionalData
		{
			if (!data)
				return false;

			var type = data.GetType();

			if (_additionalData.ContainsKey(type))
			{
				if (!overwrite)
					return false;

				_additionalData[type] = data;
			}
			else
				_additionalData.Add(
					type,
					data);

			return true;
		}
		public bool ContainsAdditionalData<DataType>()
			where DataType : AdditionalData
			=>
			_additionalData.ContainsKey(
				typeof(DataType));
		public bool RemoveAdditionalData<DataType>()
			where DataType : AdditionalData
			=>
			_additionalData.Remove(
				typeof(DataType));
		public bool TryGetData<DataType>(out DataType data)
			where DataType : AdditionalData
		{
			data = null;
			var type = typeof(DataType);

			if (!_additionalData.ContainsKey(type))
				return false;

			data = _additionalData[type] as DataType;
			return true;
		}
	}
}
