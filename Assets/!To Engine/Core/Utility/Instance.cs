
using System;

using UnityEngine;

namespace ICD.Engine.Core.Utility
{
	[RequireComponent(typeof(InstanceEventsProvider))]
	[DisallowMultipleComponent]
	public abstract class Instance : MonoBehaviour
	{
		protected bool isAwaked { get; private set; }
		protected event Action Awaked;
		internal virtual void InvokeOnAwaked()
		{
			if (isAwaked)
				return;
			isAwaked = true;
			Awaked?.Invoke();
		}

		protected event Action Started;
		internal virtual void InvokeOnStarted() =>
			Started?.Invoke();

		protected bool isDestroyed { get; private set; }
		protected event Action Destroyed;
		internal virtual void InvokeOnDestroyed()
		{
			if (isDestroyed)
				return;
			isDestroyed = true;
			Destroyed?.Invoke();
		}

		protected event Action<bool> ApplicationPauseEvent;
		internal virtual void InvokeOnApplicationPauseEvent(bool isPaused) =>
			ApplicationPauseEvent?.Invoke(isPaused);

		protected event Action<bool> ApplicationFocusEvent;
		internal virtual void InvokeOnApplicationFocusEvent(bool isFocused) =>
			ApplicationFocusEvent?.Invoke(isFocused);

		protected virtual event Action ApplicationQuited;
		internal void InvokeOnApplicationQuited() =>
			ApplicationQuited?.Invoke();
	}
}
