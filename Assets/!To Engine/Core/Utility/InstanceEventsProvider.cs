
using System;
using UnityEngine;

namespace ICD.Engine.Core.Utility
{
	[DisallowMultipleComponent]
	public sealed class InstanceEventsProvider : MonoBehaviour
	{
		private Instance _instance;
		private Instance instance
		{
			get
			{
				if (!_instance)
					_instance = GetComponent<Instance>();
				return _instance;
			}
		}
		void Awake() =>
			instance?.InvokeOnAwaked();

		void Start() =>
			instance?.InvokeOnStarted();

		void OnDestroy() =>

			instance?.InvokeOnDestroyed();

		private void OnApplicationFocus(bool isFocused) =>
			instance?.InvokeOnApplicationFocusEvent(isFocused);

		private void OnApplicationPause(bool isPaused) =>
			instance?.InvokeOnApplicationPauseEvent(isPaused);

		void OnApplicationQuit() =>
			instance?.InvokeOnApplicationQuited();
	}
}
