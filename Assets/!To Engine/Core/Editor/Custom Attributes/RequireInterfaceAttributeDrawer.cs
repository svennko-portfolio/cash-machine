
using System;

using UnityEngine;
using UnityEditor;

namespace ICD.Engine.Core.CustomAttributes
{
	[CustomPropertyDrawer(typeof(RequireInterfaceAttribute))]
	public class RequireInterfaceAttributeDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var requiredType = (attribute as RequireInterfaceAttribute).requiredType;
			if (IsValid(property, requiredType))
			{
				label.tooltip = $"Require interface: {requiredType}";
				ValidateProperty(
					property,
					requiredType);
			}

			EditorGUI.PropertyField(
				position,
				property,
				label);
		}

		private bool IsValid(SerializedProperty property, Type targetType)
		{
			return targetType.IsInterface && property.propertyType == SerializedPropertyType.ObjectReference;
		}

		private void ValidateProperty(SerializedProperty property, Type requiredType)
		{
			if (!property.objectReferenceValue)
				return;

			Action<SerializedProperty, Type> propertyValidation = null;
			switch (property.objectReferenceValue)
			{
				case GameObject:
					propertyValidation = ValidateAsGameObject;
					break;
				default:
					propertyValidation = ValidateAsObject;
					break;
			}

			propertyValidation?.Invoke(
				property,
				requiredType);
		}

		private void ValidateAsGameObject(SerializedProperty property, Type requiredType)
		{
			var component = (property.objectReferenceValue as GameObject).GetComponent(requiredType);
			if (!component)
			{
				property.objectReferenceValue = null;
				Debug.LogError(
					$"Game object must contain component implementing interface: {requiredType}");
			}
			else
				property.objectReferenceValue = component;
		}

		private void ValidateAsObject(SerializedProperty property, Type requiredType)
		{
			if (!requiredType.IsAssignableFrom(property.objectReferenceValue.GetType()))
			{
				property.objectReferenceValue = null;
				Debug.LogError(
					$"Object must implement inteface: {requiredType}");
			}
		}
	}
}
