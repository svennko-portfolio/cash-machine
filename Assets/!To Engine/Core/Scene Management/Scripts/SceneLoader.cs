﻿
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace ICD.Engine.Core.SceneManagment
{
	public class SceneLoader : MonoBehaviour
	{
		const float READY_SCENE_PROGRESS = .9f;

		private AsyncOperation _loadingOperation;
		public bool isLoading => _loadingOperation != null;

		[SerializeField]
		[Min(0)]
		private float _minimalLoadingDelay;
		public float minimalLoadingDelay
		{
			get => _minimalLoadingDelay;
			set => _minimalLoadingDelay =
				value >= 0 ?
					value :
					0;
		}

		[Space]
		public UnityEvent
			_sceneLoadingStarted,
			_sceneLoaded,
			_sceneOpened;

		[SerializeField]
		private bool _autoSceneActivation;
		public bool autoSceneActivation
		{
			get => _autoSceneActivation;
			set => _autoSceneActivation = value;
		}
		public float _loadingProgress { get; private set; }
		async Task AsyncSceneLoading(float openingDelay)
		{
			DontDestroyOnLoad(gameObject);
			_loadingOperation.allowSceneActivation = false;

			_sceneLoadingStarted?.Invoke();
			var startTime = Time.realtimeSinceStartup;

			do
				await Task.Yield();
			while (_loadingOperation.progress < READY_SCENE_PROGRESS);

			_sceneLoaded?.Invoke();

			while (
				!_loadingOperation.allowSceneActivation &&
				(!_autoSceneActivation || Time.realtimeSinceStartup - startTime < openingDelay))
				await Task.Yield();	

			_loadingOperation.allowSceneActivation = true;

			while (!_loadingOperation.isDone)
				await Task.Yield();
			_loadingOperation = null;

			_sceneOpened?.Invoke();

			System.GC.Collect(0, System.GCCollectionMode.Optimized);
		}
		async Task LoadSceneAsync(object sceneIdentificator)
		{
			if (isLoading)
				return;

			switch (sceneIdentificator)
			{
				case int sceneID:
					_loadingOperation = SceneManager.LoadSceneAsync(sceneID);
					break;
				case string sceneName:
					_loadingOperation = SceneManager.LoadSceneAsync(sceneName);
					break;
				default:
					Debug.LogError(
						$"Scene ID must be a {typeof(int)} or a {typeof(string)}.\n" +
						$"Current type: {sceneIdentificator.GetType()}");
					break;
			}

			await AsyncSceneLoading(_minimalLoadingDelay);
		}
		public async void LoadSceneAsync(int sceneIndex) =>
			await LoadSceneAsync(sceneIndex as object);
		public async Task LoadSceneTask(int sceneIndex) =>
			await LoadSceneAsync(sceneIndex as object);

		public async void LoadSceneAsync(string sceneName) =>
			await LoadSceneAsync(sceneName as object);
		public async Task LoadSceneTask(string sceneName) =>
			await LoadSceneAsync(sceneName as object);

		[ContextMenu("Activate immediate")]
		public void OpenImmediateWhenLoaded()
		{
			if (_loadingOperation != null)
				_loadingOperation.allowSceneActivation = true;
		}
		public void LoadScene(int sceneIndex) => SceneManager.LoadScene(sceneIndex);
		public void LoadScene(string scenename) => SceneManager.LoadScene(scenename);
	}
}