
using UnityEngine;

namespace ICD.Engine.Core
{
	public class WaitForSecondsAsyncOperation : CustomYieldInstruction
	{
		public bool realTime { get; }
		public float delay { get; }

		private readonly float _startTime;
		public override bool keepWaiting =>
			(realTime ?
				Time.realtimeSinceStartup :
				Time.time) -
			_startTime < delay;

		public WaitForSecondsAsyncOperation(float delay, bool realTime = true)
		{
			this.realTime = realTime;
			this.delay =
				delay > 0 ?
					delay :
					0;

			_startTime =
				realTime ?
					Time.realtimeSinceStartup :
					Time.time;
		}
	}
}
