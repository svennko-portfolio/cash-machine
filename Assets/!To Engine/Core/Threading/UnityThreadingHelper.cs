
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;

namespace ICD.Engine.Core.Threading
{
	public static partial class UnityThreadingHelper
	{
		public async static Task WaitForDelay(float duration, bool realTime, CancellationToken cancellationToken)
		{
			for (
				var startTime = realTime ? Time.realtimeSinceStartup : Time.time;
				(realTime ? Time.realtimeSinceStartup : Time.time) - startTime < duration && !cancellationToken.IsCancellationRequested;
				await Task.Yield()) ;
		}
		public async static Task WaitForDelay(float duration, bool realTime = true) =>
			await
				WaitForDelay(
					duration,
					realTime,
					CancellationToken.None);
		public static AsyncOperationAwaiter GetAwaiter(this AsyncOperation asyncOperation) =>
			new AsyncOperationAwaiter(
				asyncOperation);
	}
}