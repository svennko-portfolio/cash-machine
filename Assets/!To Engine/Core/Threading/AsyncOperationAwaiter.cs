
using System;
using System.Runtime.CompilerServices;

using UnityEngine;

namespace ICD.Engine.Core.Threading
{
	public class AsyncOperationAwaiter : INotifyCompletion
	{
		private Action _continuation;
		private AsyncOperation _operation;
		public bool IsCompleted =>
			_operation != null &&
			_operation.isDone;

		public float progress =>
			_operation != null ?
				_operation.progress :
				-1;
		public AsyncOperationAwaiter(AsyncOperation asyncOperation)
		{
			_operation = asyncOperation;
			if (_operation != null)
				_operation.completed += OnRequestCompleted;
		}
		private void OnRequestCompleted(AsyncOperation operation)
		{
			if (operation != null)
				operation.completed -= OnRequestCompleted;
			_continuation?.Invoke();
		}
		public void OnCompleted(Action continuation) =>
			_continuation = continuation;
		public void GetResult() { }
	}
	public abstract class AsyncOperationAwaiter<ResultType> : INotifyCompletion
	{
		private Action _continuation;
		private AsyncOperation _operation;
		public bool IsCompleted =>
			_operation != null &&
			_operation.isDone;

		public float progress =>
			_operation != null ?
				_operation.progress :
				-1;
		public AsyncOperationAwaiter(AsyncOperation asyncOperation)
		{
			_operation = asyncOperation;
			if (_operation != null)
				_operation.completed += OnRequestCompleted;
		}
		private void OnRequestCompleted(AsyncOperation operation)
		{
			if (operation != null)
				operation.completed -= OnRequestCompleted;
			_continuation?.Invoke();
		}
		public void OnCompleted(Action continuation) =>
			_continuation = continuation;
		public abstract ResultType GetResult();
	}
}