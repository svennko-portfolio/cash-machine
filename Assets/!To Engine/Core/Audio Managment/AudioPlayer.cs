
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using ICD.Engine.Core.Utility;

namespace ICD.Engine.Core.Audio
{
	public enum PlayMode
	{
		directOrder,
		recerseOrder,
		random
	}
	public class AudioPlayer : MonoBehaviour
	{
		[SerializeField]
		private PlayMode _playMode;
		public PlayMode playMode
		{
			get => _playMode;
			set => _playMode = value;
		}

		[SerializeField]
		private PlayList _playList;
		public PlayList playList
		{
			get => _playList;
			set
			{
				if (value != null && value.inVolumeCurve.length > 0)
					_inVolumeCurve = value.inVolumeCurve;

				if (value != null && value.outVolumeCurve.length > 0)
					_outVolumeCurve = value.outVolumeCurve;

				_currentTrackPlayer.outputAudioMixerGroup =
				_nextTrackPlayer.outputAudioMixerGroup =
				value.mixerGroup;

				_playList = value;
			}
		}

		[SerializeField]
		private bool _loop;
		public bool loop
		{
			get => _loop;
			set => _loop = value;
		}

		[SerializeField]
		[HideInInspector]
		private AudioSource
			_currentTrackPlayer,
			_nextTrackPlayer;

		[SerializeField]
		private AnimationCurve
			_inVolumeCurve,
			_outVolumeCurve;
		public AnimationCurve inVolumeCurve
		{
			get => _inVolumeCurve;
			internal set => _inVolumeCurve = value;
		}
		public AnimationCurve outVolumeCurve
		{
			get => _outVolumeCurve;
			internal set => _outVolumeCurve = value;
		}

		[SerializeField]
		private bool _autoStart;
		public bool autoStart
		{
			get => _autoStart;
			internal set => _autoStart = value;
		}

		[SerializeField]
		private float _volume;
		public float volume
		{
			get => _volume;
			set => _volume = Mathf.Clamp01(value);
		}

		public bool isPlaying { get; private set; }

		public int currentClipIndex { get; private set; } = -1;
		public async void StartPlayList(bool immediate = false)
		{
			if (!_playList)
				return;

			await Stop(immediate);

			PlayListTask();
		}
		public async Task Stop(bool immediate = false)
		{
			if (!isPlaying)
				return;
			isPlaying = false;

			if (immediate)
			{
				_currentTrackPlayer?.Stop();
				_nextTrackPlayer?.Stop();
				_currentTrackPlayer.clip =
				_nextTrackPlayer.clip =
				null;
			}
			else
				await BlendNewClipTask(null);
			currentClipIndex = -1;
		}
		async Task PlayListTask()
		{
			isPlaying = true;
			var availableIndexes = new List<int>(_playList.Count);
			do
			{
				availableIndexes.Clear();
				for (int i = 0; i < _playList.Count; i++)
					availableIndexes.Add(i);

				while (isPlaying && availableIndexes.Count > 0)
				{
					for (int i = availableIndexes.Count - 1; i >= 0; i--)
						if (availableIndexes[i] >= _playList.Count)
							availableIndexes.RemoveAt(i);

					switch (_playMode)
					{
						case PlayMode.directOrder:
							if (++currentClipIndex >= _playList.Count)
								currentClipIndex = 0;
							break;
						case PlayMode.recerseOrder:
							if (--currentClipIndex < 0)
								currentClipIndex = _playList.Count - 1;
							break;
						case PlayMode.random:
							currentClipIndex =
								availableIndexes[
									UnityEngine.Random.Range(
										0,
										availableIndexes.Count)];
							break;
					}
					availableIndexes.Remove(currentClipIndex);

					await BlendNewClipTask(_playList[currentClipIndex]);
					while (
						isPlaying &&
						_currentTrackPlayer.isPlaying &&
						_currentTrackPlayer.clip.length - _currentTrackPlayer.time > _outVolumeCurve.GetProperties().durationFromZero)
						await Task.Yield();
				}
			}
			while (loop && isPlaying && enabled);
			await Stop(false);
		}
		async Task BlendTrack(AudioSource source, AnimationCurve curve, Vector2 volumeStep)
		{
			if (!source || !source.clip)
				return;

			var curvePoperties = curve.GetProperties();

			source.volume = volumeStep[0];
			for (float startTime = Time.realtimeSinceStartup, ellapsed = 0;
				ellapsed < curvePoperties.durationFromZero;
				ellapsed = Time.realtimeSinceStartup - startTime)
			{
				if (ellapsed >= curvePoperties.startKey.time)
				{
					if (!source.isPlaying)
						source.Play();
					source.volume = curve.Evaluate(ellapsed) * _volume / curvePoperties.maxValueKey.value;
				}
				await Task.Yield();
			}
			source.volume = volumeStep[1];
		}
		private List<Task> _blendTasks = new List<Task>(2);
		async Task BlendNewClipTask(AudioClip newClip)
		{
			_blendTasks.Clear();

			if (_currentTrackPlayer.clip && _currentTrackPlayer.isPlaying)
				_blendTasks.Add(
					BlendTrack(
						_currentTrackPlayer,
						_outVolumeCurve,
						new Vector2(_volume, 0)));

			_nextTrackPlayer.Stop();
			if (newClip)
			{
				_nextTrackPlayer.clip = newClip;
				_blendTasks.Add(
					BlendTrack(
						_nextTrackPlayer,
						_inVolumeCurve,
						new Vector2(0, _volume)));
			}

			await Task.WhenAll(_blendTasks.ToArray());
			foreach (var task in _blendTasks)
				task?.Dispose();

			var player = _currentTrackPlayer;
			_currentTrackPlayer = _nextTrackPlayer;
			_nextTrackPlayer = player;

			_nextTrackPlayer.Stop();
			_nextTrackPlayer.clip = null;
		}

		[ContextMenu("Initialize Players")]
		void InitializePlayers()
		{
			var sources =
			new Queue<AudioSource>(
				GetComponents<AudioSource>());

			if (!_currentTrackPlayer)
				_currentTrackPlayer =
					sources.Count > 0 ?
						sources.Dequeue() :
						gameObject.AddComponent<AudioSource>();

			if (!_nextTrackPlayer || _nextTrackPlayer == _currentTrackPlayer)
				_nextTrackPlayer =
					sources.Count > 0 ?
						sources.Dequeue() :
						gameObject.AddComponent<AudioSource>();

			_currentTrackPlayer.playOnAwake =
			_nextTrackPlayer.playOnAwake =
			false;

			while (sources.Count > 0)
			{
				var source = sources.Dequeue();
				if (source && source != _currentTrackPlayer && source != _nextTrackPlayer)
					source.DestroyByApplicationPlayingState();
			}
		}
		void Awake()
		{
			InitializePlayers();
		}
		void Start()
		{
			if (_playList && autoStart)
			{
				playList = _playList;
				StartPlayList();
			}
		}
		void OnDestroy()
		{
			Stop();
			_currentTrackPlayer?.DestroyByApplicationPlayingState();
			_nextTrackPlayer?.DestroyByApplicationPlayingState();
		}
	}
}
