
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Audio;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using ICD.Engine.Core.Singletons;
using ICD.Engine.Core.Audio;
using ICD.Engine.Core.Utility;

namespace ICD.Engine.Core.Audio
{
	public class AudioManager : Singleton<AudioManager>
	{
		public override bool dontDestroyOnLoad => true;
		private List<AudioPlayer> _audioPlayers = new List<AudioPlayer>(3);
		public AudioPlayer CreatePersistentAudioPlayer()
		{
			var newPlayer = new GameObject($"Player ({_audioPlayers.Count + 1})").AddComponent<AudioPlayer>();
			newPlayer.transform.SetParent(transform);
			if (dontDestroyOnLoad)
				DontDestroyOnLoad(newPlayer.gameObject);
			_audioPlayers.Add(newPlayer);
			return newPlayer;
		}
		public AudioPlayer mainPlayer =>
			_audioPlayers != null && _audioPlayers.Count > 0 ?
				_audioPlayers[0] :
				null;


		public bool DestroyPlayer(AudioPlayer player)
		{
			if (_audioPlayers.Remove(player))
			{
				player.gameObject.DestroyByApplicationPlayingState();
				return true;
			}
			return false;
		}
		void Awake()
		{
			_audioPlayers.AddRange(GetComponentsInChildren<AudioPlayer>());
		}
		void OnDestroy()
		{
			foreach (var player in _audioPlayers)
				player.gameObject.DestroyByApplicationPlayingState();
		}
	}
}
