
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

namespace ICD.Engine.Core.Audio
{
    public class VolumeToggle : MonoBehaviour
    {
        [SerializeField]
        private bool _soundOn;

		public bool soundOn
		{
			get => _soundOn;
			set
			{
				AudioListener.volume =
					value ? 1 : 0;
				_soundOn = value;

				InvokeSoundStateEvents();
			}
		}
#if UNITY_EDITOR
		bool SetSound(bool soundOn)
		{
			this.soundOn = soundOn;
			return true;
		}
#endif

		[SerializeField]
		private UnityEvent
			_soundSettedOn,
			_soundSettedOff;
		void InvokeSoundStateEvents()
		{
			if (AudioListener.volume < 0.001f)
				_soundSettedOff?.Invoke();
			else
				_soundSettedOn?.Invoke();
		}
		void Awake() =>
			InvokeSoundStateEvents();
		void Update() =>
			InvokeSoundStateEvents();
	}
}
