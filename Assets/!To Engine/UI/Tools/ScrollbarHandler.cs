
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

namespace ICD.Engine.UI
{
    [DisallowMultipleComponent]
    [ExecuteAlways]
    public class ScrollbarHandler : MonoBehaviour
    {
        [SerializeField]
        [SceneObjectsOnly]
        [Required]
        private ScrollRect _scoll;
        private new RectTransform transform => base.transform as RectTransform;

		void LateUpdate()
		{
            if (!_scoll)
                return;

            transform.anchorMin =
            transform.anchorMax =
                new Vector2(
                    transform.anchorMin.x,
                    _scoll.verticalNormalizedPosition);
            transform.anchoredPosition = new Vector2(transform.anchoredPosition.x, 0);
		}
	}
}
