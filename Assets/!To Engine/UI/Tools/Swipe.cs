
using System.Collections.Generic;


using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using Sirenix.OdinInspector;

using ICD.Engine.Core.Utility;

namespace ICD.Engine.UI.Tools
{
	[RequireComponent(typeof(ScrollRect))]
	public class Swipe : MonoBehaviour
	{
		public enum PointType
		{
			normal,
			start,
			end,
			loop
		}

		[SerializeField]
		[HideInInspector]
		private List<RectTransform> _slides = new List<RectTransform>();

		[SerializeField]
		private TMP_Text _counterField;

		[SerializeField]
		private bool _loop;

		[FoldoutGroup("Events")]
		[SerializeField]
		private UnityEvent
			_normalSlideReached,
			_startReached,
			_endReached,
			_loopReached;

		[FoldoutGroup("Events")]
		[SerializeField]
		private IntegerEvent _slideSelected;

		private ScrollRect _scrollRect;
		private ScrollRect scrollRect
		{
			get
			{
				if (!_scrollRect)
					_scrollRect = GetComponent<ScrollRect>();
				return _scrollRect;
			}
		}

		public RectTransform currentSlide { get; private set; }

		private int _currentSlideIndex;

		private bool CorrectIndex(ref int index, out PointType pointType)
		{
			pointType = PointType.normal;

			if (index >= _slides.Count)
			{
				if (_loop)
				{
					index = 0;
					pointType = PointType.loop;
				}
				else
				{
					index = _slides.Count - 1;
					pointType = PointType.end;
				}
				return _loop;
			}
			if (index < 0)
			{
				if (_loop)
				{
					index = _slides.Count - 1;
					pointType = PointType.loop;
				}
				else
				{
					index = 0;
					pointType = PointType.start;
				}
				return _loop;
			}

			if (index == 0)
				pointType = PointType.start;
			else
			if (index == _slides.Count - 1)
				pointType = PointType.end;

			return true;
		}
		public int currentSlideIndex
		{
			get => _currentSlideIndex;
			set
			{
				if (_slides.Count == 0 || _currentSlideIndex == value)
					return;

				CorrectIndex(ref value, out PointType pointType);

				foreach (var slide in _slides)
					slide.SetParent(scrollRect.content.parent);
				scrollRect.content.position = _slides[value].position;
				foreach (var slide in _slides)
					slide.SetParent(scrollRect.content);

				if (_counterField)
					_counterField.text = $"{value + 1}/{_slides.Count}";

				currentSlide = _slides[value];

				var lastIndex = _currentSlideIndex;
				_currentSlideIndex = value;

				if (Mathf.Abs(lastIndex - _currentSlideIndex) > 1)
					_normalSlideReached?.Invoke();

				switch (pointType)
				{
					case PointType.normal:
						_normalSlideReached?.Invoke();
						break;
					case PointType.start:
						_startReached?.Invoke();
						break;
					case PointType.end:
						_endReached?.Invoke();
						break;
					case PointType.loop:
						_loopReached?.Invoke();
						break;
				}
				_slideSelected?.Invoke(_currentSlideIndex);
			}
		}
		public void ToNext()
		{
			var newindex = _currentSlideIndex + 1;
			if (newindex >= _slides.Count)
				if (_loop)
				{
					currentSlideIndex = 0;
					return;
				}
			currentSlideIndex = newindex;
		}
		public void ToPrevious()
		{
			var newindex = _currentSlideIndex - 1;
			if (newindex >= _slides.Count)
				if (_loop)
				{
					currentSlideIndex = 0;
					return;
				}
			currentSlideIndex = newindex;
		}
		public void AddSlide(RectTransform slide)
		{
			if (slide)
				slide.SetParent(scrollRect.content);
		}

		void Awake()
		{
			OnValidate();
			if (_slides.Count > 0)
				currentSlideIndex = _currentSlideIndex;
		}
		void OnValidate()
		{
			scrollRect.movementType = ScrollRect.MovementType.Elastic;
			//scrollRect.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, scrollRect.viewport.rect.width);
			//scrollRect.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, scrollRect.viewport.rect.height);

			_slides.Clear();
			for (int c = 0; c < scrollRect.content.childCount; c++)
				_slides.Add(
					scrollRect.content.GetChild(c) as RectTransform);
		}
	}
}
