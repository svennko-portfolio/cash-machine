
using System;

using UnityEngine;
using TMPro;
using Sirenix.OdinInspector;

namespace ICD.Engine.UI.Tools
{
	[DisallowMultipleComponent]
	[ExecuteAlways]
	public class LocalTimeSetter : MonoBehaviour
	{
		[SerializeField]
		[ChildGameObjectsOnly]
		private TMP_Text _timeField;

		[SerializeField]
		private string _format = "HH:mm";

		void Update()
		{
			if (_timeField)
				_timeField.text =
					string.IsNullOrEmpty(_format) ?
						DateTime.Now.ToString() :
						DateTime.Now.ToString(
							_format);
		}
	}
}
