
using UnityEngine;
using UnityEngine.UI;

namespace ICD.Engine.UI.Tools
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(CanvasScaler))]
	[ExecuteAlways]
	public class SmartCanvasScaler : MonoBehaviour
	{
		[SerializeField]
		private AnimationCurve _scaleCurve;

		private CanvasScaler _scaler;
		private CanvasScaler scaler
		{
			get
			{
				if (!_scaler)
					_scaler = GetComponent<CanvasScaler>();
				return _scaler;
			}
		}
		public float currentAspectRatio
		{
			get
			{
				var size = (scaler.transform as RectTransform).rect.size;
				return size.x / size.y;
			}
		}
		void LateUpdate()
		{
			scaler.matchWidthOrHeight =
				_scaleCurve.Evaluate(
					currentAspectRatio);
		}
	}
}
