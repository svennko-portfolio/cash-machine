
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace ICD.Engine.UI
{
    [RequireComponent(typeof(Button))]
    public class ButtonActivatorByInputValue : MonoBehaviour
    {
		[SerializeField]
		private TMP_InputField _inputField;

        private Button _button;
        private Button button
		{
			get
			{
                if (!_button)
                    _button = GetComponent<Button>();
                return _button;
			}
		}
		void Update()
		{
			button.interactable = _inputField && !string.IsNullOrEmpty(_inputField.text);
		}
	}
}
