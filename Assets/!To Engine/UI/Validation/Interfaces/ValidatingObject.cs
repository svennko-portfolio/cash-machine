
namespace ICD.Engine.UI.Validation.Interfaces
{
    public enum ValidationState
    {
        normal,
        valid,
        invalid
    }
    public interface ValidatingObject
    {
        public ValidationState state { get; }
        public void SetState(ValidationState state, string message = "");
        public void ResetValidationObject();
    }
    public interface ValidatingObject<ValueType> : ValidatingObject
    {
        public ValueType value { get; }
    }
}
