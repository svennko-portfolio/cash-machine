

namespace ICD.Engine.UI.Validation.Interfaces
{
    public interface Validator
    {
        public ValidationState Validate(ValidatingObject validatingObject);
    }
    public interface Validator<ValueType>
    {
        public ValidationState Validate(ValidatingObject<ValueType> validatingObject);
    }
}
