
using System;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using ICD.Engine.UI.Validation.Interfaces;

namespace ICD.Engine.UI.Validation
{
	public class ValidatingInput : MonoBehaviour, ValidatingObject<string>
	{
		[SerializeField]
		private TMP_InputField _input;

		[Serializable]
		public class InvaldStateEvent : UnityEvent<string> { }

		[SerializeField]
		private UnityEvent
			_onNormalState,
			_onValidState;

		[SerializeField]
		private InvaldStateEvent _onInvalidState;

		public ValidationState state { get; private set; }

		public string value
		{
			get
			{
				if (!_input)
					_input = GetComponentInChildren<TMP_InputField>();

				return
					_input ?
						_input.text :
						null;
			}
		}

		public void SetState(ValidationState state, string message = "")
		{
			this.state = state;
			switch (state)
			{
				case ValidationState.normal:
					_onNormalState?.Invoke();
					break;
				case ValidationState.valid:
					_onValidState?.Invoke();
					break;
				case ValidationState.invalid:
					_onInvalidState?.Invoke(message);
					break;
			}
		}
		public void ResetValidationObject()
		{
			SetState(ValidationState.normal);
			_input.text = "";
		}
	}
}
