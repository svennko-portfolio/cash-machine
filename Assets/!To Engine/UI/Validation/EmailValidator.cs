
using UnityEngine;
using ICD.Engine.UI.Validation.Interfaces;

namespace ICD.Engine.UI.Validation
{
	public class EmailValidator : BaseStringValidator
	{
		[SerializeField]
		private string _invalidFormatMessage;

		public static readonly char[] atSeparator = new char[] { '@' };
		public static readonly char[] dotSeparator = new char[] { '.' };
		public override ValidationState Validate(ValidatingObject<string> validatingObject)
		{
			if (base.Validate(validatingObject) == ValidationState.invalid)
				return ValidationState.invalid;

			var value = validatingObject.value;
			if (string.IsNullOrEmpty(value))
			{
				validatingObject.SetState(ValidationState.invalid, _emptyMessage);
				return ValidationState.invalid;
			}

			var stringParts =
				value.Split(
					atSeparator,
					System.StringSplitOptions.RemoveEmptyEntries);
			if (stringParts.Length != 2)
			{
				validatingObject.SetState(ValidationState.invalid, _invalidFormatMessage);
				return ValidationState.invalid;
			}

			stringParts =
				stringParts[1].Split(
					dotSeparator,
					System.StringSplitOptions.RemoveEmptyEntries);
			if (stringParts.Length != 2)
			{
				validatingObject.SetState(ValidationState.invalid, _invalidFormatMessage);
				return ValidationState.invalid;
			}

			return ValidationState.valid;
		}
	}
}
