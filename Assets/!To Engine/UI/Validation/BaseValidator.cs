using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ICD.Engine.UI.Validation.Interfaces;

namespace ICD.Engine.UI.Validation
{
	public abstract class BaseValidator : MonoBehaviour, Validator
	{
		public abstract ValidationState Validate(ValidatingObject validatingObject);
		public abstract ValidationState Validate();
	}
}
