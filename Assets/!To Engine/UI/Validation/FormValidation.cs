
using UnityEngine;
using UnityEngine.UI;
using ICD.Engine.UI.Validation.Interfaces;

namespace ICD.Engine.UI.Validation
{
	public class FormValidation : BaseValidator, Validator, ValidatingObject
	{
		[SerializeField]
		private Component[] _validatingFields;

		[SerializeField]
		private Text _invalidMessage;

		public ValidationState state { get; set; }

		public void ResetValidationObject()
		{
			_invalidMessage.text = string.Empty;
		}

		public void SetState(ValidationState state, string message = "")
		{
			if (_invalidMessage && state == ValidationState.invalid)
				_invalidMessage.text = message;
			this.state = state;
		}

		public override ValidationState Validate(ValidatingObject validatingObject)
		{
			var valid = true;
			foreach (var field in _validatingFields)
			{
				var validator = field as BaseValidator;
				if (validator && validator.Validate() == ValidationState.invalid)
				{
					validatingObject?.SetState(ValidationState.invalid);
					valid &= false;
				}
			}
			return
				valid ?
					ValidationState.valid :
					ValidationState.invalid;
		}

		public override ValidationState Validate() =>
			Validate(this);

		public void ValidateForm() =>
			Validate();
	}
}
