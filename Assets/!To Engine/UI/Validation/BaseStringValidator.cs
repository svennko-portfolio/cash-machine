
using UnityEngine;
using ICD.Engine.UI.Validation.Interfaces;

namespace ICD.Engine.UI.Validation
{
    [RequireComponent(typeof(ValidatingObject<string>))]
    public class BaseStringValidator : BaseValidator, Validator<string>
	{
		[SerializeField]
		protected bool _checkIfEmpty;

		[SerializeField]
		protected string _emptyMessage;

		protected ValidatingObject<string> _validatinObject;
		public virtual ValidationState Validate(ValidatingObject<string> validatingObject)
		{
			if(_checkIfEmpty && string.IsNullOrEmpty(validatingObject.value))
			{
				validatingObject.SetState(ValidationState.invalid, _emptyMessage);
				return ValidationState.invalid;
			}
			validatingObject.SetState(ValidationState.valid);
			return ValidationState.valid;
		}
		[ContextMenu("Validate")]
		public override ValidationState Validate()
		{
			if (_validatinObject == null)
				_validatinObject = GetComponentInChildren<ValidatingObject<string>>();
			return Validate(_validatinObject);
		}

		public override ValidationState Validate(ValidatingObject validatingObject) =>
			Validate(_validatinObject);

		private void Awake()
		{
			_validatinObject = GetComponent<ValidatingObject<string>>();
		}
	}
}
