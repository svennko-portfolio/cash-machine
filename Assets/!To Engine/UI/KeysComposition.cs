
using System;
using UnityEngine;

namespace ICD.Engine.UI
{
	public enum KeyEventType
	{
		down,
		up,
		always
	}
	[Serializable]
	public struct KeysComposition
	{
		[SerializeField]
		private KeyCode[] _additionalKeys;
		public KeyCode[] additionalKeys
		{
			get => additionalKeys;
			set => additionalKeys = value;
		}

		[SerializeField]
		private KeyCode _mainKey;
		public KeyCode mainKey
		{
			get => _mainKey;
			set => _mainKey = value;
		}

		[SerializeField]
		private KeyEventType _mainKeyEventType;
		public KeyEventType mainKeyEventType
		{
			get => _mainKeyEventType;
			set => _mainKeyEventType = value;
		}
		bool CheckInputEvent()
		{
			if (_additionalKeys != null && _additionalKeys.Length > 0)
				foreach (var key in _additionalKeys)
					if (!Input.GetKey(key))
						return false;

			switch (_mainKeyEventType)
			{
				case KeyEventType.down:
					return Input.GetKeyDown(_mainKey);
				case KeyEventType.up:
					return Input.GetKeyUp(_mainKey);
				case KeyEventType.always:
				default:
					return Input.GetKey(_mainKey);
			}
		}
		public static implicit operator bool(KeysComposition keysComposition) =>
			keysComposition.CheckInputEvent();
	}
}
