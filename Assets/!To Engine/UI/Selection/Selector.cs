
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace ICD.Engine.UI
{
	[RequireComponent(typeof(EventSystem))]
	public class Selector : MonoBehaviour
	{
		[SerializeField]
		private KeysComposition[]
			_nextKeys,
			_previousKeys;

		private Selectable _lastSelected;

		private EventSystem eventSystem;

		void Awake()
		{
			eventSystem = GetComponent<EventSystem>();
			if (!eventSystem)
				eventSystem = EventSystem.current;
		}

		Selectable FindFirst()
		{
			var result = eventSystem.firstSelectedGameObject.GetComponentInChildren<Selectable>();
			if (result)
				return result;

			var selectables = FindObjectsOfType<Selectable>();

			foreach (var selectable in selectables)
				if (selectable && selectable.interactable)
					return selectable;

			return null;
		}
		private void Select(Selectable selectable)
		{
			if (!selectable)
				return;

			selectable.Select();

			_lastSelected = selectable;
		}
		void Update()
		{
			var curentSelectable =
				EventSystem.current.currentSelectedGameObject ?
					EventSystem.current.currentSelectedGameObject.GetComponent<Selectable>() :
					_lastSelected ?? FindFirst();

			var navigation = curentSelectable?.GetComponent<NextPreviousNavigation>();
			if (navigation)
			{
				var found = false;

				if (!found && _nextKeys != null && _nextKeys.Length > 0)
					foreach (var keyComposition in _nextKeys)
						if (keyComposition)
						{
							curentSelectable = navigation.nextSelectable;
							found = true;
							break;
						}

				if (!found && _previousKeys != null && _previousKeys.Length > 0)
					foreach (var keyComposition in _previousKeys)
						if (keyComposition)
						{
							curentSelectable = navigation.nextSelectable;
							found = true;
							break;
						}
			}

			Select(curentSelectable ?? FindFirst());
		}
	}
}
