
using UnityEngine;
using UnityEngine.UI;

namespace ICD.Engine.UI
{
    public class NextPreviousNavigation : MonoBehaviour
    {
        [SerializeField]
        private Selectable
            _nextSelectable,
            _previousSelectable;

        public Selectable nextSelectable => _nextSelectable;
        public Selectable previousSelectable => _previousSelectable;
    }
}
