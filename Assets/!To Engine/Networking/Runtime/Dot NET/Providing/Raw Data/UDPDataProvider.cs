
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using ICD.Networking.Utility;
using ICD.Networking.Providing.Interfaces;

namespace ICD.Networking.Providing.RawData
{
	public class UDPDataProvider : NetworkProvider<byte[], IPEndPoint>
	{
		public static implicit operator bool(UDPDataProvider provider) =>
			provider != null;

		private UdpClient _listener;

		private TaskScheduler _synchronizer;

		private bool _isListening;

		public UDPDataProvider()
		{
			ID = Guid.NewGuid();
		}
		public UDPDataProvider(Guid providerID)
		{
			ID = providerID;
		}

		async void Listen()
		{
			error = null;
			try
			{
				while (_isListening)
				{
					var reciveTask = _listener.ReceiveAsync();

					if (synchronizeWithContext)
						await reciveTask.ContinueWith(
							task => { },
							_synchronizer);
					else
						await reciveTask.ConfigureAwait(false);

					if (reciveTask.IsCompleted && !reciveTask.IsFaulted)
						DataReceived?.Invoke(
							this,
							new DataReceivedEventArguments<byte[], IPEndPoint>(
								reciveTask.Result.Buffer,
								reciveTask.Result.RemoteEndPoint));
				}
			}
			catch (ObjectDisposedException) { }
			catch (Exception exception)
			{
				error = exception;
			}

			Stop();
		}

		#region Network Provider
		public event EventHandler<DataReceivedEventArguments<byte[], IPEndPoint>> DataReceived;
		public Guid ID { get; }
		public Exception error { get; private set; }
		public bool canSendWhileNotListening { get; set; }

		public void Send(byte[] data, IPEndPoint destination)
		{
			if (!(isRunning || canSendWhileNotListening))
				throw new InvalidOperationException("Can send only when provider is listening");

			using (var sender = new UdpClient())
				sender.Send(
					data,
					data.Length,
					destination);
		}


		#region Listener
		public bool isRunning => _listener != null && _isListening;
		public ushort listeningPort =>
			(ushort)(
			_listener != null ?
				(_listener.Client.LocalEndPoint as IPEndPoint).Port :
				0);
		public bool synchronizeWithContext
		{
			get => _synchronizer != null;
			set =>
				_synchronizer =
					value ?
						TaskScheduler.FromCurrentSynchronizationContext() :
						null;
		}

		public void Start(ushort port, bool findClosesAvailablePort = false)
		{
			if (isRunning &&
				_listener != null && (_listener.Client.LocalEndPoint as IPEndPoint).Port == port)
				return;

			Stop();

			if (findClosesAvailablePort)
				IPEndPointHelper.CorrectForAvailableUDPPort(ref port);
			_listener = new UdpClient(port);

			_isListening = true;
			Listen();
		}
		public void Stop()
		{
			_isListening = false;

			_listener?.Dispose();
			_listener = null;
		}

		#region IDisposable
		public void Dispose() =>
			Stop();
		#endregion
		#endregion
		#endregion
	}
}