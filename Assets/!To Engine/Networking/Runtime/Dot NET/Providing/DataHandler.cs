
using System.Net;

namespace ICD.Networking.Providing
{
	public abstract class DataHandler<BaseDataType, EndPointType>
		where EndPointType : EndPoint
	{
		public abstract void Invoke(object sender, BaseDataType data, EndPointType from);
	}
}
