
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using ICD.Networking.Providing.Interfaces;
using ICD.Networking.Providing.Messaging.Interfaces;
using ICD.Networking.Providing.Messaging.Messages;

namespace ICD.Networking.Providing.Messaging
{
	public class RequestsAndMessagesProvider<EndPointType> : MessagesProvider<EndPointType>, RequestsProvider<EndPointType>
		where EndPointType : EndPoint
	{

		private Dictionary<Delegate, Delegate> _requestsHandlers = new Dictionary<Delegate, Delegate>();

		private CancellationTokenSource _disposeCancellation = new CancellationTokenSource();

		public RequestsAndMessagesProvider(NetworkProvider<byte[], EndPointType> provider) :
			base(provider)
		{ }
		~RequestsAndMessagesProvider() =>
			Dispose();

		public void RemoveRequestHandler<RequestType, ResponseType>(Delegate requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response
		{
			if (requestHandler == null || !_requestsHandlers.ContainsKey(requestHandler))
				return;

			var messageHandler = _requestsHandlers[requestHandler];
			RemoveMessageHandler(messageHandler as EventHandler<DataReceivedEventArguments<RequestType, EndPointType>>);
			_requestsHandlers.Remove(requestHandler);
		}

		#region RequestsProvider
		public NetworkSideIdentificator<EndPointType> networkSideIdentificator { get; set; }
		void SendResponse<RequestType, ResponseType>(ResponseType response, DataReceivedEventArguments<RequestType, EndPointType> requestArguments)
			where RequestType : Request<ResponseType>
			where ResponseType : Response
		{
			if (!response)
				return;

			response.requestID = requestArguments.data.ID;

			if (networkSideIdentificator == null ||
				!networkSideIdentificator.IdentificateEndPoint(
					in requestArguments,
					out EndPointType destination))
				destination = requestArguments.from;

			Send(
				response,
				destination);
		}
		public void AddRequestHandler<RequestType, ResponseType>(RequestHandler<RequestType, ResponseType, EndPointType> requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response
		{
			if (requestHandler == null || _requestsHandlers.ContainsKey(requestHandler))
				return;

			EventHandler<DataReceivedEventArguments<RequestType, EndPointType>> messageHandler =
				(sender, requestArguments) =>
				{
					if (!isRunning)
						return;

					SendResponse(
						requestHandler?.Invoke(
							this,
							requestArguments),
						requestArguments);
				};

			AddMessageHandler(messageHandler);
			_requestsHandlers.Add(requestHandler, messageHandler);
		}
		public void RemoveRequestHandler<RequestType, ResponseType>(RequestHandler<RequestType, ResponseType, EndPointType> requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response =>
			RemoveRequestHandler<RequestType, ResponseType>(requestHandler as Delegate);
		public void AddRequestHandler<RequestType, ResponseType>(AwaitableRequestHandler<RequestType, ResponseType, EndPointType> requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response
		{
			if (requestHandler == null || _requestsHandlers.ContainsKey(requestHandler))
				return;

			EventHandler<DataReceivedEventArguments<RequestType, EndPointType>> messageHandler =
				async (sender, requestArguments) =>
				{
					if (!isRunning)
						return;

					SendResponse(
						await requestHandler.Invoke(
							this,
							requestArguments),
						requestArguments);
				};

			AddMessageHandler(messageHandler);
			_requestsHandlers.Add(requestHandler, messageHandler);
		}
		public void RemoveRequestHandler<RequestType, ResponseType>(AwaitableRequestHandler<RequestType, ResponseType, EndPointType> requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response =>
			RemoveRequestHandler<RequestType, ResponseType>(requestHandler as Delegate);
		public async Task<ResponseType> SendRequest<ResponseType>(Request<ResponseType> request, EndPointType destination, CancellationToken cancellationToken)
			where ResponseType : Response
		{
			if (!request)
				throw
					new ArgumentNullException(
					"request",
					"Request must NOT be NULL");

			cancellationToken =
				CancellationTokenSource.CreateLinkedTokenSource(
					_disposeCancellation.Token,
					cancellationToken).Token;

			ResponseType response = null;

			EventHandler<DataReceivedEventArguments<ResponseType, EndPointType>> responseHandler =
				(object sender, DataReceivedEventArguments<ResponseType, EndPointType> responseArguments) =>
				{
					if (!responseArguments.data.requestID.Equals(request.ID))
						return;

					response = responseArguments.data;
				};

			try
			{
				AddMessageHandler(responseHandler);
				Send(request, destination);

				while (isRunning && response == null)
				{
					cancellationToken.ThrowIfCancellationRequested();
					await Task.Yield();
				}				
			}
			finally
			{
				RemoveMessageHandler(responseHandler);
			}

			return response;
		}
		#region IDisposable
		public override void Dispose()
		{
			_disposeCancellation.Cancel();
			base.Dispose();
			_requestsHandlers.Clear();
		}
		#endregion
		#endregion
	}
}
