
using System.Net;
using ICD.Networking.Providing.Messaging.Messages;

namespace ICD.Networking.Providing.Messaging
{
	public class ConstantEndPointIdentificator<EndPointType> : Interfaces.NetworkSideIdentificator<EndPointType>
		where EndPointType : EndPoint
	{
		public EndPointType defaultEndPoint { get; }
		public ConstantEndPointIdentificator(EndPointType defaultEndPoint) =>
			this.defaultEndPoint = defaultEndPoint;

		public bool IdentificateEndPoint<MessageType>(in DataReceivedEventArguments<MessageType, EndPointType> messageArguments, out EndPointType endPoint)
			where MessageType : Message
		{
			endPoint = defaultEndPoint;
			return true;
		}
	}
}
