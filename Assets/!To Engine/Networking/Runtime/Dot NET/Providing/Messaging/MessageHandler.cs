
using System;
using System.Net;
using ICD.Networking.Providing.Messaging.Messages;

namespace ICD.Networking.Providing.Messaging
{
	public abstract class BaseMessageHandler<EndPointType> : DataHandler<Message, EndPointType>
		where EndPointType : EndPoint
	{ }
	public class MessageHandler<MessageType, EndPointType> : BaseMessageHandler<EndPointType>
		where MessageType : Message
		where EndPointType : EndPoint
	{
		private EventHandler<DataReceivedEventArguments<MessageType, EndPointType>> _handler;
		public MessageHandler(EventHandler<DataReceivedEventArguments<MessageType, EndPointType>> handler) =>
			_handler = handler;
		public override void Invoke(object sender, Message message, EndPointType from)
		{
			var castedMessage = message as MessageType;
			if (castedMessage)
				_handler?.Invoke(
					sender,
					new DataReceivedEventArguments<MessageType, EndPointType>(
						castedMessage,
						from));
		}
	}
}
