
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using ICD.Networking.Providing.Interfaces;
using ICD.Networking.Providing.Messaging.Messages;

namespace ICD.Networking.Providing.Messaging
{
	public class MessagesProvider<EndPointType> : Interfaces.MessagesProvider<EndPointType>
		where EndPointType : EndPoint
	{
		private NetworkProvider<byte[], EndPointType> _provider;

		private BinaryFormatter _serializer = new BinaryFormatter();

		public MessagesProvider(NetworkProvider<byte[], EndPointType> provider)
		{
			_provider = provider;
		}
		~MessagesProvider() =>
			Dispose();

		void DataReceivedHandler(object sender, DataReceivedEventArguments<byte[], EndPointType> dataArguments)
		{
			Message message = null;

			using (var stream = new MemoryStream())
			{
				stream.Write(dataArguments.data, 0, dataArguments.data.Length);
				stream.Seek(0, SeekOrigin.Begin);
				message = _serializer.Deserialize(stream) as Message;
			}

			if (!message)
				return;

			DataReceived?.Invoke(
				this,
				new DataReceivedEventArguments<Message, EndPointType>(
					message,
					dataArguments.from));

			for (var messageType = message.GetType(); messageType != typeof(Message).BaseType; messageType = messageType.BaseType)
				lock (_handlersLocker)
					if (_messageHandlers.ContainsKey(messageType))
						foreach (var handler in _messageHandlers[messageType].Values)
							handler?.Invoke(
								this,
								message,
								dataArguments.from);
		}

		#region Messages Provider
		private object _handlersLocker = new object();

		private Dictionary<Type, Dictionary<Delegate, BaseMessageHandler<EndPointType>>> _messageHandlers =
			new Dictionary<Type, Dictionary<Delegate, BaseMessageHandler<EndPointType>>>();
		public void AddMessageHandler<MessageType>(EventHandler<DataReceivedEventArguments<MessageType, EndPointType>> handler)
			where MessageType : Message
		{
			var messageType = typeof(MessageType);

			lock (_handlersLocker)
			{
				if (!_messageHandlers.ContainsKey(messageType))
					_messageHandlers.Add(messageType, new Dictionary<Delegate, BaseMessageHandler<EndPointType>>());

				if (!_messageHandlers[messageType].ContainsKey(handler))
					_messageHandlers[messageType].Add(
						handler,
						new MessageHandler<MessageType, EndPointType>(
							handler));
			}
		}
		public void RemoveMessageHandler<MessageType>(Delegate handler) where MessageType : Message
		{
			var messageType = typeof(MessageType);

			lock (_handlersLocker)
			{
				if (!_messageHandlers.ContainsKey(messageType) ||
				!_messageHandlers[messageType].ContainsKey(handler))
					return;

				_messageHandlers[messageType].Remove(handler);
			}
		}
		public void RemoveMessageHandler<MessageType>(EventHandler<DataReceivedEventArguments<MessageType, EndPointType>> handler)
			where MessageType : Message
			=>
			RemoveMessageHandler<MessageType>(handler as Delegate);

		#region NetworkProvider
		public event EventHandler<DataReceivedEventArguments<Message, EndPointType>> DataReceived;
		public Guid ID =>
			_provider != null ?
				_provider.ID :
				Guid.Empty;
		public Exception error => _provider?.error;
		public bool canSendWhileNotListening
		{
			get =>
				_provider != null ?
					_provider.canSendWhileNotListening :
					false;
			set
			{
				if (_provider != null)
					_provider.canSendWhileNotListening = value;
			}
		}
		public void Send(Message message, EndPointType destination)
		{
			message.senderID = ID;
			using (var stream = new MemoryStream())
			{
				_serializer.Serialize(stream, message);
				stream.Seek(0, SeekOrigin.Begin);

				var data = new byte[stream.Length];
				stream.Read(data, 0, data.Length);

				_provider.Send(
					data,
					destination);
			}
		}

		#region Listener
		public ushort listeningPort =>
			_provider != null ?
				_provider.listeningPort :
				(ushort)0;
		public bool isRunning => _provider != null && _provider.isRunning;

		public bool synchronizeWithContext
		{
			get => _provider != null && _provider.synchronizeWithContext;
			set
			{
				if (_provider != null)
					_provider.synchronizeWithContext = value;
			}
		}
		public void Start(ushort port, bool findClosesAvailablePort = false)
		{
			_provider?.Start(
				port,
				findClosesAvailablePort);

			if (_provider != null)
				_provider.DataReceived += DataReceivedHandler;
		}
		public void Stop()
		{
			_provider?.Stop();
			if (_provider != null)
				_provider.DataReceived -= DataReceivedHandler;
		}

		#region IDisposable
		public virtual void Dispose()
		{
			Stop();
			_provider?.Dispose();
		}
		#endregion
		#endregion
		#endregion
		#endregion
	}
}
