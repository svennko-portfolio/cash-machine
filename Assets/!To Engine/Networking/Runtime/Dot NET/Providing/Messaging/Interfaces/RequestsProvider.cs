
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using ICD.Networking.Providing.Interfaces;
using ICD.Networking.Providing.Messaging.Messages;

namespace ICD.Networking.Providing.Messaging.Interfaces
{
	public delegate ResponseType RequestHandler<RequestType, ResponseType, EndPointType>(object sender, DataReceivedEventArguments<RequestType, EndPointType> requestArguments)
		where RequestType : Request<ResponseType>
		where ResponseType : Response
		where EndPointType : EndPoint;

	public delegate Task<ResponseType> AwaitableRequestHandler<RequestType, ResponseType, EndPointType>(object sender, DataReceivedEventArguments<RequestType, EndPointType> requestArguments)
		where RequestType : Request<ResponseType>
		where ResponseType : Response
		where EndPointType : EndPoint;

	public interface RequestsProvider<EndPointType> : NetworkProvider<Message, EndPointType>
		where EndPointType : EndPoint
	{
		public NetworkSideIdentificator<EndPointType> networkSideIdentificator { get; set; } 
		public void AddRequestHandler<RequestType, ResponseType>(RequestHandler<RequestType, ResponseType, EndPointType> requestHandler)
		where RequestType : Request<ResponseType>
		where ResponseType : Response;
		public void RemoveRequestHandler<RequestType, ResponseType>(RequestHandler<RequestType, ResponseType, EndPointType> requestHandler)
		where RequestType : Request<ResponseType>
			where ResponseType : Response;
		public void AddRequestHandler<RequestType, ResponseType>(AwaitableRequestHandler<RequestType, ResponseType, EndPointType> requestHandler)
		where RequestType : Request<ResponseType>
		where ResponseType : Response;
		public void RemoveRequestHandler<RequestType, ResponseType>(AwaitableRequestHandler<RequestType, ResponseType, EndPointType> requestHandler)
		where RequestType : Request<ResponseType>
			where ResponseType : Response;
		public Task<ResponseType> SendRequest<ResponseType>(Request<ResponseType> request, EndPointType destination, CancellationToken cancellationToken)
			where ResponseType : Response;
	}
}