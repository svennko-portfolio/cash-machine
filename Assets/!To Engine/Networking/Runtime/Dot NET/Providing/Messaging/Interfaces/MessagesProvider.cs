
using System;
using System.Net;
using ICD.Networking.Providing.Interfaces;
using ICD.Networking.Providing.Messaging.Messages;

namespace ICD.Networking.Providing.Messaging.Interfaces
{
	public interface MessagesProvider<EndPointType> : NetworkProvider<Message, EndPointType>
		where EndPointType : EndPoint
	{
		public void AddMessageHandler<MessageType>(EventHandler<DataReceivedEventArguments<MessageType, EndPointType>> handler)
			where MessageType : Message;
		public void RemoveMessageHandler<MessageType>(EventHandler<DataReceivedEventArguments<MessageType, EndPointType>> handler)
			where MessageType : Message;
		public void RemoveMessageHandler<MessageType>(Delegate handler)
			where MessageType : Message;
	}
}