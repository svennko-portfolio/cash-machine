
using System.Net;
using ICD.Networking.Providing.Messaging.Messages;

namespace ICD.Networking.Providing.Messaging.Interfaces
{
	public interface NetworkSideIdentificator<EndPointType>
		where EndPointType : EndPoint
	{
		public bool IdentificateEndPoint<MessageType>(in DataReceivedEventArguments<MessageType, EndPointType> messageArguments, out EndPointType sideEndPoint)
			where MessageType : Message;
	}
}
