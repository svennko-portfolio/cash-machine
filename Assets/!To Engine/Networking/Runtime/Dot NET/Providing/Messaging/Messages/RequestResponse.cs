
using System;

namespace ICD.Networking.Providing.Messaging.Messages
{
	[Serializable]
	public abstract class Request<ResponseType> : Message
		where ResponseType : Response
	{
		public override string ToString() =>
			$"Request typof {GetType().Name}. �: {ID}";
	}

	[Serializable]
	public abstract class Response : Message
	{
		public Guid requestID { get; internal set; }
		public Exception error { get; }
		public Response(Exception error = null)
		{
			this.error = error;
		}
		public override string ToString() =>
			$"Response typof {GetType().Name} for request �: {requestID}. �: {ID}";
	}
}
