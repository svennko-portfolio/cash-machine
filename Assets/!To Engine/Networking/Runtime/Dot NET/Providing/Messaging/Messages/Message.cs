
using System;

namespace ICD.Networking.Providing.Messaging.Messages
{
	[Serializable]
	public class Message
	{
		public Guid ID { get; } = Guid.NewGuid();
		public Guid senderID { get; internal set; }
		public override string ToString() =>
			$"Message �: {ID}";

		public static implicit operator bool(Message message) =>
			message != null;
	}
}
