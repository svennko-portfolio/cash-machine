
using System;
using System.Net;

namespace ICD.Networking.Providing
{
	[Serializable]
	public abstract class DataReceivedEventArguments : EventArgs
	{
		public object data { get; }
		public EndPoint from { get; internal set; }
		public DataReceivedEventArguments(object data, EndPoint from)
		{
			this.data = data;
			this.from = from;
		}
		public override string ToString() =>
			$"{data.GetType().Name} received from {from}:\n" +
			data;
	}

	[Serializable]
	public class DataReceivedEventArguments<DataType, EndPointType> : DataReceivedEventArguments
		where EndPointType : EndPoint
	{
		public new DataType data => (DataType)base.data;
		public new EndPointType from
		{
			get => base.from as EndPointType;
			internal set => base.from = value;
		}
		public DataReceivedEventArguments(DataType data, EndPointType from) :
			base(data, from)
		{ }
	}
}
