
using System;
using System.Net;

namespace ICD.Networking.Providing.Interfaces
{
    public interface NetworkProvider<DataType, EndPointType> : Listener
        where EndPointType : EndPoint
    {
        public event EventHandler<DataReceivedEventArguments<DataType, EndPointType>> DataReceived;
        public Guid ID { get; }
        public Exception error { get; }
        public bool canSendWhileNotListening { get; set; }
        public void Send(DataType data, EndPointType destination);
    }
}
