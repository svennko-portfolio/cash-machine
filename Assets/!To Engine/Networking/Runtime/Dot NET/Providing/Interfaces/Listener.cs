
using System;

namespace ICD.Networking.Providing.Interfaces
{
	public interface Listener : IDisposable
	{
		public bool isRunning { get; }
		public ushort listeningPort { get; }
		public bool synchronizeWithContext { get; set; }
		public void Start(ushort port, bool findClosesAvailablePort = false);
		public void Stop();
	}
}