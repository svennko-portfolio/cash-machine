
using System.Net;
using ICD.Networking.Providing.NetworkSides.Messages;

namespace ICD.Networking.Providing.NetworkSides
{
	public class IPServerSideIdentificator : Server<IPEndPoint>.ClientIdentificator
	{
		protected override IPEndPoint GetConnectionEndPoint(ConnectionRequest<IPEndPoint> connectionRequest, IPEndPoint from) =>
			new IPEndPoint(
				from.Address,
				connectionRequest.listeningPort);
	}
}
