
using System;
using System.Collections.Generic;
using System.Net;
using ICD.Networking.Providing.Interfaces;
using ICD.Networking.Providing.Messaging;
using ICD.Networking.Providing.Messaging.Interfaces;
using ICD.Networking.Providing.Messaging.Messages;

namespace ICD.Networking.Providing.NetworkSides
{
	public abstract class NetworkSideProvider<EndPointType> : Listener
		where EndPointType : EndPoint
	{
		public abstract event EventHandler<ConnectionEventArguments<EndPointType>> ConnectionEvent;

		protected RequestsAndMessagesProvider<EndPointType> _provider;

		private Dictionary<Delegate, Delegate> _handlers = new Dictionary<Delegate, Delegate>();

		public NetworkSideProvider(NetworkProvider<byte[], EndPointType> dataProvider, NetworkSideIdentificator<EndPointType> identificator = null)
		{
			_provider =
				new RequestsAndMessagesProvider<EndPointType>(
					dataProvider);
			_provider.networkSideIdentificator = identificator;
			_provider.canSendWhileNotListening = false;
		}
		protected void AddMessageHandler<MessageType>(Delegate messageHandler, EventHandler<DataReceivedEventArguments<MessageType, EndPointType>> nativeHandler)
			where MessageType : Message
		{
			if (messageHandler == null || _handlers.ContainsKey(messageHandler))
				return;

			_provider.AddMessageHandler(nativeHandler);
			_handlers.Add(
				messageHandler,
				nativeHandler);
		}
		public void RemoveMessageHandler<MessageType>(Delegate messageHandler)
			where MessageType : Message
		{
			if (!_handlers.ContainsKey(messageHandler))
				return;

			_provider.RemoveMessageHandler<MessageType>(_handlers[messageHandler]);
			_handlers.Remove(messageHandler);
		}
		protected void AddRequestHandler<RequestType, ResponseType>(Delegate requestHandler, Delegate nativeHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response
		{
			if (requestHandler == null || _handlers.ContainsKey(requestHandler))
				return;

			switch (nativeHandler)
			{
				case RequestHandler<RequestType, ResponseType, EndPointType> castedHandler:
					_provider.AddRequestHandler(castedHandler);
					break;
				case AwaitableRequestHandler<RequestType, ResponseType, EndPointType> castedHandler:
					_provider.AddRequestHandler(castedHandler);
					break;
			}
			_handlers.Add(
				requestHandler,
				nativeHandler);
		}
		public void RemoveRequestHandler<RequestType, ResponseType>(Delegate requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response
		{
			if (!_handlers.ContainsKey(requestHandler))
				return;

			switch (_handlers[requestHandler])
			{
				case RequestHandler<RequestType, ResponseType, EndPointType> castedHandler:
					_provider.RemoveRequestHandler(castedHandler);
					break;
				case AwaitableRequestHandler<RequestType, ResponseType, EndPointType> castedHandler:
					_provider.RemoveRequestHandler(castedHandler);
					break;
			}
			_handlers.Remove(requestHandler);
		}
		#region Listener
		public bool isRunning => _provider.isRunning;
		public ushort listeningPort => _provider.listeningPort;
		public bool synchronizeWithContext
		{
			get => _provider.synchronizeWithContext;
			set => _provider.synchronizeWithContext = value;
		}
		public abstract void Start(ushort port, bool findClosesAvailablePort = false);
		public abstract void Stop();
		#region IDisposable
		public void Dispose()
		{
			Stop();
			_provider.Dispose();
		}
		#endregion
		#endregion
	}
}
