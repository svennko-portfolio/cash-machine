
using System;
using System.Net;
using ICD.Networking.Providing.Messaging.Messages;

namespace ICD.Networking.Providing.NetworkSides.Messages
{
	[Serializable]
	public class ConnectionRequest<EndPointType> : Request<ConnectionResponse<EndPointType>>
		where EndPointType : EndPoint
	{
		public ushort listeningPort { get; }

		public ConnectionRequest(ushort listeningPort)
		{
			this.listeningPort = listeningPort;
		}
	}

	[Serializable]
	public class ConnectionResponse<EndPointType> : Response
		where EndPointType : EndPoint
	{
		public enum Result
		{
			successful,
			already,
			clientsLimit,
			error
		}
		public Result result { get; }
		public EndPointType requesterEndPoint { get; }

		public ConnectionResponse(Result result, EndPointType yourEndPoint, Exception error = null) :
			base(error)
		{
			this.result = result;
		}
	}
}
