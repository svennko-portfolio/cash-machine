
using System;
using ICD.Networking.Providing.Messaging.Messages;

namespace ICD.Networking.Providing.NetworkSides.Messages
{
	[Serializable]
	public class DisconnectionMessage : Message
	{ }
}
