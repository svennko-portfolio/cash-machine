
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using ICD.Networking.Providing.NetworkSides.Messages;
using ICD.Networking.Providing.Interfaces;
using ICD.Networking.Providing.Messaging;
using ICD.Networking.Providing.Messaging.Interfaces;
using ICD.Networking.Providing.Messaging.Messages;
using ICD.Networking.Providing.NetworkSides.Interfaces;

namespace ICD.Networking.Providing.NetworkSides
{
	public class Client<EndPointType> : NetworkSideProvider<EndPointType>, Interfaces.Client<EndPointType>
		where EndPointType : EndPoint
	{
		public Client(NetworkProvider<byte[], EndPointType> dataProvider) :
			base(dataProvider)
		{ }
		void DisconnectionHandler(object sender, DataReceivedEventArguments<DisconnectionMessage, EndPointType> messageArguments) =>
			DisconnectFromServer(false);

		#region Client
		public override event EventHandler<ConnectionEventArguments<EndPointType>> ConnectionEvent;

		public NetworkSide<EndPointType> selfSide { get; private set; }
		public NetworkSide<EndPointType> server { get; private set; }
		public bool isConnected =>
			isRunning &&
			!server.Equals(NetworkSide<EndPointType>.Empty);
		public async Task ConnectToServer(EndPointType serverEndPoint, ushort listeningPort, CancellationToken cancellationToken, bool findClosesAvailablePort = false)
		{
			if (isConnected && listeningPort == _provider.listeningPort && serverEndPoint.Equals(server.endPoint))
				return;

			DisconnectFromServer(true);

			_provider.Start(
				listeningPort,
				findClosesAvailablePort);
			_provider.networkSideIdentificator =
				new ConstantEndPointIdentificator<EndPointType>(
					serverEndPoint);

			var response = await
				_provider.SendRequest(
					new ConnectionRequest<EndPointType>(
						_provider.listeningPort),
					serverEndPoint,
					cancellationToken);

			if (response == null)
			{
				_provider.Stop();
				throw new Exception("Connection response is NULL");
			}
			if (response.result != ConnectionResponse<EndPointType>.Result.successful)
			{
				_provider.Stop();
				if (response.error != null)
					throw response.error;
				return;
			}

			_provider.AddMessageHandler<DisconnectionMessage>(DisconnectionHandler);
			server = new NetworkSide<EndPointType>()
			{
				ID = response.senderID,
				endPoint = serverEndPoint
			};
			selfSide = new NetworkSide<EndPointType>()
			{
				ID = _provider.ID,
				endPoint = response.requesterEndPoint
			};
			ConnectionEvent?.Invoke(
				this,
				new ConnectionEventArguments<EndPointType>(
					true,
					server));
		}
		void DisconnectFromServer(bool notificateServer)
		{
			if (!isConnected)
				return;

			if (notificateServer)
				_provider.Send(
					new DisconnectionMessage(),
					server.endPoint);

			_provider.RemoveMessageHandler<DisconnectionMessage>(DisconnectionHandler);

			ConnectionEvent?.Invoke(
				this,
				new ConnectionEventArguments<EndPointType>(
					false,
					server));

			server =
			selfSide = NetworkSide<EndPointType>.Empty;

			_provider.Stop();
		}
		public void DisconnectFromServer() =>
			DisconnectFromServer(true);

		public void AddMessageHandler<MessageType>(EventHandler<MessageType> messageHandler)
			where MessageType : Message
			=>
			AddMessageHandler(
				messageHandler,
				new EventHandler<DataReceivedEventArguments<MessageType, EndPointType>>(
					(sender, messageArguments) =>
					{
						if (!isConnected ||
							!server.ID.Equals(messageArguments.data.senderID))
							return;
						messageHandler?.Invoke(
							this,
							messageArguments.data);
					}));

		public void RemoveMessageHandler<MessageType>(EventHandler<MessageType> messageHandler)
			where MessageType : Message
			=>
			RemoveMessageHandler<MessageType>(messageHandler as Delegate);
		public void Send(Message message)
		{
			if (!isConnected)
				throw new Exception("Client is NOT connected to server");

			_provider.Send(
				message,
				server.endPoint);
		}
		public void AddRequestHandler<RequestType, ResponseType>(ClientRequestHandler<RequestType, ResponseType> requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response
			=>
			AddRequestHandler<RequestType, ResponseType>(
				requestHandler,
				new RequestHandler<RequestType, ResponseType, EndPointType>(
					(sender, requestArguments) =>
					{
						if (!isConnected ||
							!server.ID.Equals(requestArguments.data.senderID))
							return null;
						return
							requestHandler?.Invoke(
								this,
								requestArguments.data);
					}));
		public void AddRequestHandler<RequestType, ResponseType>(AwaitableClientRequestHandler<RequestType, ResponseType> requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response
			=>
			AddRequestHandler<RequestType, ResponseType>(
				requestHandler,
				new AwaitableRequestHandler<RequestType, ResponseType, EndPointType>(
					async (sender, requestArguments) =>
					{
						if (!isConnected ||
							!server.ID.Equals(requestArguments.data.senderID))
							return null;
						return await
							requestHandler?.Invoke(
								this,
								requestArguments.data);
					}));
		public void RemoveRequestHandler<RequestType, ResponseType>(ClientRequestHandler<RequestType, ResponseType> requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response
			=>
			RemoveRequestHandler<RequestType, ResponseType>(
				requestHandler as Delegate);
		public void RemoveRequestHandler<RequestType, ResponseType>(AwaitableClientRequestHandler<RequestType, ResponseType> requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response
			=>
			RemoveRequestHandler<RequestType, ResponseType>(
				requestHandler as Delegate);
		public async Task<ResponseType> SendRequest<ResponseType>(Request<ResponseType> request, CancellationToken cancellationToken)
			where ResponseType : Response
		{
			if (!isConnected)
				throw new Exception("Client is NOT connected to server");

			try
			{
				return await
					_provider.SendRequest(
						request,
						server.endPoint,
						cancellationToken);
			}
			catch (OperationCanceledException)
			{
				DisconnectFromServer(false);
			}
			return null;
		}

		#region Listener
		public override void Start(ushort port, bool findClosesAvailablePort = false) =>
			throw new NotImplementedException();

		public override void Stop() =>
			DisconnectFromServer();
		#endregion
		#endregion
	}
}