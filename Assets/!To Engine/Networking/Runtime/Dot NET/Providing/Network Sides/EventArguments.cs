
using System;
using System.Net;
using ICD.Networking.Providing.Messaging.Messages;

namespace ICD.Networking.Providing.NetworkSides
{
	[Serializable]
	public class ConnectionEventArguments<EndPointType> : EventArgs
		where EndPointType : EndPoint
	{
		public bool connectOrDisconnect { get; }
		public NetworkSide<EndPointType> side { get; }
		public ConnectionEventArguments(bool connectOrDisconnect, NetworkSide<EndPointType> side)
		{
			this.connectOrDisconnect = connectOrDisconnect;
			this.side = side;
		}
		public override string ToString() =>
			$"{side} {(connectOrDisconnect ? "connected" : "disconnected")}";
	}

	[Serializable]
	public class ClentMessageEventArguments : EventArgs
	{
		public Message message { get; }
		public Guid clientID { get; }
		public ClentMessageEventArguments(Message message, Guid clientID)
		{
			this.message = message;
			this.clientID = clientID;
		}
	}

	[Serializable]
	public class ServerMessageEventArguments<MessageType> : ClentMessageEventArguments
		where MessageType : Message
	{
		public new MessageType message => base.message as MessageType;
		public ServerMessageEventArguments(MessageType message, Guid clientID) :
			base(message, clientID)
		{ }
	}
}
