
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using ICD.Networking.Providing.NetworkSides.Messages;
using ICD.Networking.Providing.Interfaces;
using ICD.Networking.Providing.Messaging;
using ICD.Networking.Providing.Messaging.Interfaces;
using ICD.Networking.Providing.Messaging.Messages;
using ICD.Networking.Providing.NetworkSides.Interfaces;
using ICD.Networking.Utility;

namespace ICD.Networking.Providing.NetworkSides
{
	public sealed class Server<EndPointType> : NetworkSideProvider<EndPointType>, Interfaces.Server<EndPointType>
		where EndPointType : EndPoint
	{
		private Dictionary<Guid, EndPointType> _clients = new Dictionary<Guid, EndPointType>();

		public Guid ID => _provider.ID;
		public abstract class ClientIdentificator : NetworkSideIdentificator<EndPointType>
		{
			public Server<EndPointType> server { get; set; }
			protected abstract EndPointType GetConnectionEndPoint(ConnectionRequest<EndPointType> connectionRequest, EndPointType from);
			public bool IdentificateEndPoint<MessageType>(in DataReceivedEventArguments<MessageType, EndPointType> messageArguments, out EndPointType sideEndPoint)
				where MessageType : Message
			{
				sideEndPoint = default;
				if (server == null || messageArguments == null)
					return false;

				if (server._clients.TryGetValue(messageArguments.data.senderID, out sideEndPoint))
					return true;
				else
				if (messageArguments.data is ConnectionRequest<EndPointType>)
				{
					sideEndPoint = GetConnectionEndPoint(messageArguments.data as ConnectionRequest<EndPointType>, messageArguments.from);
					return true;
				}
				return false;
			}
		}
		public Server(NetworkProvider<byte[], EndPointType> dataProvider, ClientIdentificator identificator) :
			base(dataProvider, identificator)
		{
			identificator.server = this;
		}
		ConnectionResponse<EndPointType> ConnectionRequestHandler(object sender, DataReceivedEventArguments<ConnectionRequest<EndPointType>, EndPointType> requestArguments)
		{
			var result = ConnectionResponse<EndPointType>.Result.successful;
			Exception error = null;

			if (_provider.networkSideIdentificator.IdentificateEndPoint(in requestArguments, out EndPointType clientEndPoint))
			{
				if (_clients.ContainsKey(requestArguments.data.senderID))
					result = ConnectionResponse<EndPointType>.Result.already;
				else
				if (_clients.Count >= maxClientsCount)
					error = new Exception("Max connected clients count reached");
				else
					_clients.Add(
						requestArguments.data.senderID,
						clientEndPoint);
			}
			else
				error = new Exception("Can not identificate client");

			if (error != null)
				result = ConnectionResponse<EndPointType>.Result.error;
			else
				ConnectionEvent?.Invoke(
					this,
					new ConnectionEventArguments<EndPointType>(
						true,
						new NetworkSide<EndPointType>()
						{
							ID = requestArguments.data.senderID,
							endPoint = clientEndPoint
						}));
			return
				new ConnectionResponse<EndPointType>(
					result,
					clientEndPoint,
					error);
		}
		void DisconnectionHandler(object sender, DataReceivedEventArguments<DisconnectionMessage, EndPointType> messageArguments) =>
			DisconnectClient(
				messageArguments.data.senderID,
				false);
		public void DisconnectClient(Guid clientID, bool notifyClient)
		{
			if (!_clients.TryGetValue(clientID, out EndPointType clientEndPoint))
				return;

			if (notifyClient)
				Send(new DisconnectionMessage(), clientID);

			_clients.Remove(clientID);

			ConnectionEvent?.Invoke(
				this,
				new ConnectionEventArguments<EndPointType>(
					false,
					new NetworkSide<EndPointType>()
					{
						ID = clientID,
						endPoint = clientEndPoint
					}));
		}

		#region Server
		public override event EventHandler<ConnectionEventArguments<EndPointType>> ConnectionEvent;
		public IEnumerable<Guid> clients => _clients.Keys;
		public uint maxClientsCount { get; set; }
		public uint clientsCount => (uint)_clients.Count;

		public bool IsClientConnected(Guid clientID) =>
			_clients.ContainsKey(clientID);
		public void DisconnectClient(Guid clientID) =>
			DisconnectClient(
				clientID,
				true);
		public void DisconnectAll()
		{
			var clientsIDs = new Guid[_clients.Count];
			_clients.Keys.CopyTo(clientsIDs, 0);
			foreach (var clientID in clientsIDs)
				DisconnectClient(clientID);
		}
		public void AddMessageHandler<MessageType>(EventHandler<ServerMessageEventArguments<MessageType>> messageHandler)
			where MessageType : Message
			=>
			AddMessageHandler(
				messageHandler,
				new EventHandler<DataReceivedEventArguments<MessageType, EndPointType>>(
					(sender, messageArguments) =>
					{
						if (!_provider.networkSideIdentificator.IdentificateEndPoint(
								in messageArguments,
								out EndPointType clientEndPoint))
							return;
						messageHandler?.Invoke(
							this,
							new ServerMessageEventArguments<MessageType>(
								messageArguments.data,
								messageArguments.data.senderID));
					}));
		public void RemoveMessageHandler<MessageType>(EventHandler<ServerMessageEventArguments<MessageType>> messageHandler)
			where MessageType : Message
			=>
			RemoveMessageHandler<MessageType>(messageHandler as Delegate);
		public void Send(Message message, Guid clientID)
		{
			if (!_clients.ContainsKey(clientID))
				throw new Exception($"Unknown client: {clientID}");

			_provider.Send(
				message,
				_clients[clientID]);
		}
		public void SendToAll(Message message)
		{
			foreach (var client in _clients.Keys)
				Send(message, client);
		}
		public void AddRequestHandler<RequestType, ResponseType>(ServerRequestHandler<RequestType, ResponseType> requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response
			=>
			AddRequestHandler<RequestType, ResponseType>(
				requestHandler,
				new RequestHandler<RequestType, ResponseType, EndPointType>(
					(sender, requestArguments) =>
					{
						if (!_provider.networkSideIdentificator.IdentificateEndPoint(
								in requestArguments,
								out EndPointType clientEndPoint))
							return null;
						return requestHandler?.Invoke(
							this,
							new ServerMessageEventArguments<RequestType>(
								requestArguments.data,
								requestArguments.data.senderID));
					}));
		public void AddRequestHandler<RequestType, ResponseType>(AwaitableServerRequestHandler<RequestType, ResponseType> requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response
			=>
			AddRequestHandler<RequestType, ResponseType>(
				requestHandler,
				new AwaitableRequestHandler<RequestType, ResponseType, EndPointType>(
					async (sender, requestArguments) =>
					{
						if (!_provider.networkSideIdentificator.IdentificateEndPoint(
								in requestArguments,
								out EndPointType clientEndPoint))
							return null;
						return await
							requestHandler?.Invoke(
								this,
								new ServerMessageEventArguments<RequestType>(
									requestArguments.data,
									requestArguments.data.senderID));
					}));
		public void RemoveRequestHandler<RequestType, ResponseType>(ServerRequestHandler<RequestType, ResponseType> requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response
			=>
			RemoveRequestHandler<RequestType, ResponseType>(
				requestHandler as Delegate);
		public void RemoveRequestHandler<RequestType, ResponseType>(AwaitableServerRequestHandler<RequestType, ResponseType> requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response
			=>
			RemoveRequestHandler<RequestType, ResponseType>(
				requestHandler as Delegate);
		public async Task<ResponseType> SendRequest<ResponseType>(Request<ResponseType> request, Guid clientID, CancellationToken cancellationToken)
			where ResponseType : Response
		{
			if (!_clients.ContainsKey(clientID))
				throw new Exception($"Unknown client: {clientID}");

			try
			{
				return await
					_provider.SendRequest(
						request,
						_clients[clientID],
						cancellationToken);
			}
			catch (OperationCanceledException)
			{
				DisconnectClient(clientID, false);
			}
			return null;
		}

		#region Listener
		public override void Start(ushort port, bool findClosesAvailablePort = false)
		{
			_provider.Start(
				port,
				findClosesAvailablePort);
			_provider.AddRequestHandler<ConnectionRequest<EndPointType>, ConnectionResponse<EndPointType>>(ConnectionRequestHandler);
			_provider.AddMessageHandler<DisconnectionMessage>(DisconnectionHandler);
		}

		public override void Stop()
		{
			_provider.RemoveRequestHandler<ConnectionRequest<EndPointType>, ConnectionResponse<EndPointType>>(ConnectionRequestHandler);
			_provider.RemoveMessageHandler<DisconnectionMessage>(DisconnectionHandler);
			DisconnectAll();
			_provider.Stop();
		}
		#endregion
		#endregion
	}
}
