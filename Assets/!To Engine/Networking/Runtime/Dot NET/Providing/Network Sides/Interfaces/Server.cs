
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using ICD.Networking.Providing.Interfaces;
using ICD.Networking.Providing.Messaging.Messages;

namespace ICD.Networking.Providing.NetworkSides.Interfaces
{
	public interface Server<EndPointType> : Listener
		where EndPointType : EndPoint
	{
		public event EventHandler<ConnectionEventArguments<EndPointType>> ConnectionEvent;
		public IEnumerable<Guid> clients { get; }
		public uint maxClientsCount { get; set; }
		public uint clientsCount { get; }
		public bool IsClientConnected(Guid clientID);
		public void DisconnectClient(Guid clientID);
		public void DisconnectAll();

		public void AddMessageHandler<MessageType>(EventHandler<ServerMessageEventArguments<MessageType>> messageHandler)
			where MessageType : Message;
		public void Send(Message message, Guid clientID);
		public void SendToAll(Message message);
		public void RemoveMessageHandler<MessageType>(EventHandler<ServerMessageEventArguments<MessageType>> messageHandler)
			where MessageType : Message;
		public void AddRequestHandler<RequestType, ResponseType>(ServerRequestHandler<RequestType, ResponseType> requestHandler)
		where RequestType : Request<ResponseType>
		where ResponseType : Response;
		public void RemoveRequestHandler<RequestType, ResponseType>(ServerRequestHandler<RequestType, ResponseType> requestHandler)
		where RequestType : Request<ResponseType>
			where ResponseType : Response;
		public void AddRequestHandler<RequestType, ResponseType>(AwaitableServerRequestHandler<RequestType, ResponseType> requestHandler)
		where RequestType : Request<ResponseType>
		where ResponseType : Response;
		public void RemoveRequestHandler<RequestType, ResponseType>(AwaitableServerRequestHandler<RequestType, ResponseType> requestHandler)
		where RequestType : Request<ResponseType>
			where ResponseType : Response;
		public void RemoveRequestHandler<RequestType, ResponseType>(Delegate requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response;
		public Task<ResponseType> SendRequest<ResponseType>(Request<ResponseType> request, Guid clientID, CancellationToken cancellationToken)
			where ResponseType : Response;
	}
}
