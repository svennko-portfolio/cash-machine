
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using ICD.Networking.Providing.Interfaces;
using ICD.Networking.Providing.Messaging.Messages;
using ICD.Networking.Providing.NetworkSides.Messages;

namespace ICD.Networking.Providing.NetworkSides.Interfaces
{
	public interface Client<EndPointType> : Listener
		where EndPointType : EndPoint
	{
		public event EventHandler<ConnectionEventArguments<EndPointType>> ConnectionEvent;
		public NetworkSide<EndPointType> server { get; }
		public NetworkSide<EndPointType> selfSide { get; }
		public bool isConnected { get; }
		public Task ConnectToServer(EndPointType serverEndPoint, ushort listeningPort, CancellationToken cancellationToken, bool findClosesAvailablePort = false);
		public void DisconnectFromServer();

		public void AddMessageHandler<MessageType>(EventHandler<MessageType> messageHandler)
			where MessageType : Message;
		public void RemoveMessageHandler<MessageType>(EventHandler<MessageType> messageHandler)
			where MessageType : Message;
		public void Send(Message message);
		public void AddRequestHandler<RequestType, ResponseType>(ClientRequestHandler<RequestType, ResponseType> requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response;
		public void RemoveRequestHandler<RequestType, ResponseType>(ClientRequestHandler<RequestType, ResponseType> requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response;
		public void AddRequestHandler<RequestType, ResponseType>(AwaitableClientRequestHandler<RequestType, ResponseType> requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response;
		public void RemoveRequestHandler<RequestType, ResponseType>(AwaitableClientRequestHandler<RequestType, ResponseType> requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response;
		public void RemoveRequestHandler<RequestType, ResponseType>(Delegate requestHandler)
			where RequestType : Request<ResponseType>
			where ResponseType : Response;
		public Task<ResponseType> SendRequest<ResponseType>(Request<ResponseType> request, CancellationToken cancellationToken)
			where ResponseType : Response;
	}
}
