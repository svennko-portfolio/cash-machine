
using System.Threading.Tasks;

using ICD.Networking.Providing.Messaging.Messages;

namespace ICD.Networking.Providing.NetworkSides
{
	public delegate ResponseType ServerRequestHandler<RequestType, ResponseType>(object sender, ServerMessageEventArguments<RequestType> requestArguments)
		where RequestType : Request<ResponseType>
		where ResponseType : Response;

	public delegate Task<ResponseType> AwaitableServerRequestHandler<RequestType, ResponseType>(object sender, ServerMessageEventArguments<RequestType> requestArguments)
		where RequestType : Request<ResponseType>
		where ResponseType : Response;
	public delegate ResponseType ClientRequestHandler<RequestType, ResponseType>(object sender, RequestType request)
		where RequestType : Request<ResponseType>
		where ResponseType : Response;

	public delegate Task<ResponseType> AwaitableClientRequestHandler<RequestType, ResponseType>(object sender, RequestType request)
		where RequestType : Request<ResponseType>
		where ResponseType : Response;
}
