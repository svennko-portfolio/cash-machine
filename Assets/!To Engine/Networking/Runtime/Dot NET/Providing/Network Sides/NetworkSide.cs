﻿
using System;
using System.Collections.Generic;
using System.Net;

namespace ICD.Networking.Providing.NetworkSides
{
	[Serializable]
	public struct NetworkSide<EndPointType> : IEquatable<NetworkSide<EndPointType>>
		where EndPointType : EndPoint
	{
		public static readonly NetworkSide<EndPointType> Empty =
			new NetworkSide<EndPointType>()
			{
				ID = Guid.Empty,
				endPoint = null
			};
		public static implicit operator KeyValuePair<Guid, EndPointType>(NetworkSide<EndPointType> side)=>
			new KeyValuePair<Guid, EndPointType>(
				side.ID,
				side.endPoint);
		public static NetworkSide<EndPointType> FromKeyValuePair(KeyValuePair<Guid, EndPointType> pair) =>
			new NetworkSide<EndPointType>()
			{
				ID = pair.Key,
				endPoint = pair.Value
			};
		public Guid ID { get; set; }
		public EndPointType endPoint { get; set; }
		public override bool Equals(object otherObject) =>
			otherObject is NetworkSide<EndPointType> &&
			Equals((NetworkSide<EndPointType>)otherObject);
		public bool Equals(NetworkSide<EndPointType> other) =>
			other.ID.Equals(ID);
		public override int GetHashCode() =>
			ID.GetHashCode();
		public override string ToString() =>
			$"№: {ID}. End point: {endPoint}";
	}
}