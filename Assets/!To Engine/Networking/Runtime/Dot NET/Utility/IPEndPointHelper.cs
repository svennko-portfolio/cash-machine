
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using ICD.Networking;

namespace ICD.Networking.Utility
{
	public static class IPEndPointHelper
	{
		[Flags]
		public enum EndPointFilteringMode
		{
			none = 0,
			address = 1 << 0,
			port = 1 << 1,
			full = address | port
		}
		[Serializable]
		public struct EndPointsFilter
		{
			public EndPointFilteringMode checkMode { get; set; }
			public HashSet<IPEndPoint> validEndPoints { get; set; }
		}
		public static bool CheckByFilter(this IPEndPoint endPoint, EndPointsFilter filter)
		{
			if (filter.validEndPoints == null || filter.validEndPoints.Count == 0)
				return true;

			foreach (var filterEndPoint in filter.validEndPoints)
			{
				var valid = true;

				if (filter.checkMode.HasFlag(EndPointFilteringMode.address))
					valid &= filterEndPoint.Address.Equals(IPAddress.Any) || endPoint.Address.Equals(filterEndPoint.Address);

				if (filter.checkMode.HasFlag(EndPointFilteringMode.port))
					valid &= endPoint.Port == filterEndPoint.Port;

				if (valid)
					return true;
			}

			return false;
		}
		public static IPAddress localHostAddress { get; } =
			new IPAddress(
				new byte[] { 127, 0, 0, 1 });
		public static IPEndPoint GetLocalHostEndPoint(ushort port) =>
			new IPEndPoint(
				localHostAddress,
				port);

		public static HashSet<ushort> capturedUDPPorts =>
			new HashSet<ushort>(
				from endPoint in IPGlobalProperties.GetIPGlobalProperties().GetActiveUdpListeners()
				select (ushort)endPoint.Port);

		public static bool IsAvailableUDPPort(ushort port) =>
			!capturedUDPPorts.Contains(port);
		public static bool CorrectForAvailableUDPPort(ref ushort port)
		{
			var capturedPorts = capturedUDPPorts;

			if (!capturedPorts.Contains(port))
				return true;

			for (ushort d = 1; d < ushort.MaxValue / 2; d++)
			{
				var testPort = (ushort)(port + d);
				if (!capturedPorts.Contains(testPort))
				{
					port = testPort;
					return true;
				}

				testPort = (ushort)(port - d);
				if (!capturedPorts.Contains(testPort))
				{
					port = testPort;
					return true;
				}
			}

			return false;
		}
	}
}
