
using System;

using Sirenix.OdinInspector;

using UnityEngine;

namespace ICD.Engine.Networking.Utility
{
	[CreateAssetMenu(fileName = "Host Settings", menuName = "ICD/Engine/Networking/Host Settings", order = 51)]
	public class HostSettings : ScriptableObject
	{
		[SerializeField]
		private Scheme _scheme = Scheme.HTTP;
		public string scheme => _scheme.ToString();

		[SerializeField]
		[ValidateInput("CheckHostname", "Invalid hostname")]
		private string _hostname;
		public string hostname => _hostname;
		bool CheckHostname(string hostname) =>
			Uri.CheckHostName(
				_hostname) != UriHostNameType.Unknown;

		[SerializeField]
		private ushort _port;
		public ushort port => _port;

		[SerializeField]
		private string _path;
		public string path => _path;

		[SerializeField]
		private string _userName;
		public string userName => _userName;

		[SerializeField]
		private string _password;
		public string password => _password;

		private UriBuilder _URIBuilder = new UriBuilder();
		public Uri hostUri
		{
			get
			{
				if (!CheckHostname(_hostname))
					return null;

				_URIBuilder.Scheme = scheme;
				_URIBuilder.Host = _hostname;
				_URIBuilder.Port = _port;
				_URIBuilder.Path = _path;
				_URIBuilder.UserName = _userName;
				_URIBuilder.Password = _password;

				_URIBuilder.Query = null;
				_URIBuilder.Fragment = null;

				return _URIBuilder.Uri;
			}
		}
	}
}
