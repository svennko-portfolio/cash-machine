
using System;
using System.Collections.Generic;

namespace ICD.Engine.Networking.Utility
{
	public enum Scheme
	{
		Delimiter,
		File,
		FTP,
		Goper,
		HTTP,
		HTTPS,
		MailTo,
		NetPipe,
		NetTCP,
		News,
		NNTP
	}
	public static class URIHelper
	{
		public static IReadOnlyDictionary<Scheme, string> schemas { get; } =
			new Dictionary<Scheme, string>()
			{
				{Scheme.Delimiter, Uri.SchemeDelimiter},
				{Scheme.File, Uri.UriSchemeFile},
				{Scheme.FTP, Uri.UriSchemeFtp},
				{Scheme.Goper, Uri.UriSchemeGopher},
				{Scheme.HTTP, Uri.UriSchemeHttp},
				{Scheme.HTTPS, Uri.UriSchemeHttps},
				{Scheme.MailTo, Uri.UriSchemeMailto},
				{Scheme.NetPipe, Uri.UriSchemeNetPipe},
				{Scheme.NetTCP, Uri.UriSchemeNetTcp},
				{Scheme.News, Uri.UriSchemeNews},
				{Scheme.NNTP, Uri.UriSchemeNntp}
			};
		public static IReadOnlyDictionary<Scheme, ushort?> defaultPorts { get; } =
			new Dictionary<Scheme, ushort?>()
			{
				{Scheme.Delimiter, null},
				{Scheme.File, null},
				{Scheme.FTP, 21},
				{Scheme.Goper, 70},
				{Scheme.HTTP, 80},
				{Scheme.HTTPS, 443},
				{Scheme.MailTo, 25},
				{Scheme.NetPipe, null},
				{Scheme.NetTCP, 808},
				{Scheme.News, null},
				{Scheme.NNTP, 119}
			};
	}
}
