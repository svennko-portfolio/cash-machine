
using System;
using System.Collections.Generic;
using System.Threading;
using System.Net;
using UnityEngine;
using UnityEngine.Events;
using ICD.Networking.Providing;
using ICD.Networking.Providing.NetworkSides;
using ICD.Networking.Providing.RawData;
using ICD.Networking.Tests.Providing.Messaging.Messages;

namespace ICD.Networking.Tests.Providing.ClentServer
{
	[ExecuteAlways]
	public class ServerTest : MonoBehaviour
	{
		[SerializeField]
		private ushort _selfPort = 33333;

		[SerializeField]
		private uint _maxClientsCount = 2;

		[SerializeField]
		[Min(0)]
		protected int _targetClientIndex;

		[SerializeField]
		[Range(.1f, 3600f)]
		private float _timeout = 5;

		[Header("State Monitoring")]
		[SerializeField]
		private bool _isRunning;

		[SerializeField]
		private uint _clientsCount;

		[SerializeField]
		private List<string> _clients = new List<string>();

		[Header("Events")]
		private UnityEvent
			_onRequestReceived,
			_onResponseReceived;

		private Networking.Providing.NetworkSides.Interfaces.Server<IPEndPoint> _server =
			new Server<IPEndPoint>(
				new UDPDataProvider(),
				new IPServerSideIdentificator());

		void ConnectionHandler(object sender, ConnectionEventArguments<IPEndPoint> connectionArgumnets)
		{
			if (connectionArgumnets.connectOrDisconnect)
				_clients.Add(connectionArgumnets.side.ID.ToString());
			else
				_clients.Remove(connectionArgumnets.side.ID.ToString());
			print($"Client {(connectionArgumnets.connectOrDisconnect ? "connected" : "disconnected")}: {connectionArgumnets.side} ");

			ShowClients();
		}

		[ContextMenu("Start Server")]
		void StartServer()
		{
			if (_server.isRunning && _server.listeningPort == _selfPort)
				return;

			_server.Start(
				_selfPort,
				true);

			_server.AddRequestHandler<TestRequest1, TestResponse>(TestRequestHandler);
			_server.AddRequestHandler<TestRequest2, TestResponse>(TestRequestHandler);

			print($"Server started on port {_server.listeningPort}");
		}

		[ContextMenu("Stop Server")]
		void StopServer()
		{
			if (!_server.isRunning)
				return;
			_server.Stop();

			_server.RemoveRequestHandler<TestRequest1, TestResponse>(TestRequestHandler);
			_server.RemoveRequestHandler<TestRequest2, TestResponse>(TestRequestHandler);

			print($"Server stopped");
		}

		[ContextMenu("Show Clients")]
		void ShowClients()
		{
			var clientsString = $"Total clients: {_server.clientsCount}";
			foreach (var client in _server.clients)
				clientsString += $"\n{client}";
			print(clientsString);
		}

		[ContextMenu("Check Client")]
		void CheckClients()
		{
			print($"Client connection status: {_server.IsClientConnected(Guid.Parse(_clients[_targetClientIndex]))}");
		}

		[ContextMenu("Disconnect Client")]
		void DisconnectClient()
		{
			_server.DisconnectClient(
				Guid.Parse(
					_clients[_targetClientIndex]));
		}

		[ContextMenu("Disconnect All Clients")]
		void DisconnectAllClients()
		{
			_server.DisconnectAll();
		}
		TestResponse TestRequestHandler(object sender, ServerMessageEventArguments<TestRequest1> requestArguments)
		{
			print("Request handler 1");
			_onRequestReceived?.Invoke();
			return new TestResponse();
		}
		TestResponse TestRequestHandler(object sender, ServerMessageEventArguments<TestRequest2> requestArguments)
		{
			print("Request handler 2");
			_onRequestReceived?.Invoke();
			return new TestResponse();
		}
		[ContextMenu("Send Request 1")]
		void SendRequest1() =>
			SendTestRequest<TestRequest1>();

		[ContextMenu("Send Request 2")]
		void SendRequest2() =>
			SendTestRequest<TestRequest2>();
		async void SendTestRequest<RequestType>()
			where RequestType : TestRequest1, new()
		{
			var response = await _server.SendRequest(
				new RequestType(),
				Guid.Parse(
					_clients[_targetClientIndex]),
				new CancellationTokenSource(
					(int)(_timeout * 1000)).Token);

			print($"Response received: {response}");
			if (response)
				_onResponseReceived?.Invoke();
		}

		void Awake()
		{
			_server.synchronizeWithContext = true;
			_server.ConnectionEvent += ConnectionHandler;
		}
		void OnDestroy()
		{
			_server.Dispose();
			_server.ConnectionEvent -= ConnectionHandler;
		}
		void OnValidate()
		{
			if (_server.isRunning)
				_selfPort = _server.listeningPort;
			_isRunning = _server.isRunning;
			_clientsCount = _server.clientsCount;
			_server.maxClientsCount = _maxClientsCount;
		}
	}
}
