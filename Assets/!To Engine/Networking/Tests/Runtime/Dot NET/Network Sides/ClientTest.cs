
using System.Threading;
using System.Net;
using UnityEngine;
using UnityEngine.Events;
using ICD.Networking.Providing.NetworkSides;
using ICD.Networking.Providing.RawData;
using ICD.Networking.Tests.Providing.Messaging.Messages;

namespace ICD.Networking.Tests.Providing.ClentServer
{
	[ExecuteAlways]
	public class ClientTest : MonoBehaviour
	{
		[SerializeField]
		private ushort _selfPort = 22222;

		[SerializeField]
		private byte[] _serverIP = new byte[] { 127, 0, 0, 1 };
		[SerializeField]
		private ushort _serverPort = 33333;

		[SerializeField]
		[Range(.1f, 3600f)]
		private float _timeout = 5;

		[Header("State Monitoring")]
		[SerializeField]
		private bool _isConnected;

		[Header("Events")]
		private UnityEvent
			_onRequestReceived,
			_onResponseReceived;

		protected IPEndPoint server =>
			new IPEndPoint(
				new IPAddress(
					_serverIP),
				_serverPort);

		private Networking.Providing.NetworkSides.Interfaces.Client<IPEndPoint> _client =
			new Client<IPEndPoint>(
				new UDPDataProvider());

		void ConnectionHandler(object sender, ConnectionEventArguments<IPEndPoint> connectionArgumnets)
		{
			if (connectionArgumnets.connectOrDisconnect)
			{
				_client.AddRequestHandler<TestRequest1, TestResponse>(TestRequestHandler);
				_client.AddRequestHandler<TestRequest2, TestResponse>(TestRequestHandler);
			}
			else
			{
				_client.RemoveRequestHandler<TestRequest1, TestResponse>(TestRequestHandler);
				_client.RemoveRequestHandler<TestRequest2, TestResponse>(TestRequestHandler);
			}
			print($"Client on {_client.listeningPort} {(connectionArgumnets.connectOrDisconnect ? "connected to" : "disconnected from")} server: {connectionArgumnets.side}");
		}

		[ContextMenu("Connect To Server")]
		async void ConnectToServer()
		{
			if (_client.isConnected)
				return;
			await _client.ConnectToServer(
				server,
				_selfPort,
				new CancellationTokenSource(
					(int)(_timeout * 1000)).Token,
				true);
		}

		[ContextMenu("Disconnect From Server")]
		void DisconnectFromServer()
		{
			if (!_client.isConnected)
				return;
			_client.DisconnectFromServer();
		}
		TestResponse TestRequestHandler(object sender, TestRequest1 request)
		{
			print("Request handler 1");
			_onRequestReceived?.Invoke();
			return new TestResponse();
		}
		TestResponse TestRequestHandler(object sender, TestRequest2 request)
		{
			print("Request handler 2");
			_onRequestReceived?.Invoke();
			return new TestResponse();
		}
		[ContextMenu("Send Request 1")]
		void SendRequest1() =>
			SendTestRequest<TestRequest1>();

		[ContextMenu("Send Request 2")]
		void SendRequest2() =>
			SendTestRequest<TestRequest2>();
		async void SendTestRequest<RequestType>()
			where RequestType : TestRequest1, new()
		{
			var response = await _client.SendRequest(
				new RequestType(),
				new CancellationTokenSource(
					(int)(_timeout * 1000)).Token);

			print($"Response received: {response}");
			if (response)
				_onResponseReceived?.Invoke();
		}

		void Awake()
		{
			_client.synchronizeWithContext = true;
			_client.ConnectionEvent += ConnectionHandler;
		}
		void OnDestroy()
		{
			_client.Dispose();
			_client.ConnectionEvent -= ConnectionHandler;
		}
		void OnValidate()
		{
			if (_client.isConnected)
				_selfPort = _client.listeningPort;
			_isConnected = _client.isConnected;
		}
	}
}
