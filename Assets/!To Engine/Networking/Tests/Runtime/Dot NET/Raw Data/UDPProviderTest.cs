
using System.Net;
using UnityEngine;
using ICD.Networking.Providing;
using ICD.Networking.Providing.RawData;

namespace ICD.Networking.Tests.Providing.RawData
{
	public class UDPProviderTest : MonoBehaviour
	{
		[SerializeField]
		private ushort _port;

		[SerializeField]
		private byte[] _targetIP = new byte[] { 127, 0, 0, 1 };
		[SerializeField]
		private short _targetPort = 22222;

		UDPDataProvider _client = new UDPDataProvider()
		{
			canSendWhileNotListening = false
		};

		void DataReceivedHandler(object sender, DataReceivedEventArguments<byte[], IPEndPoint> eventArguments)
		{
			print($"Received {eventArguments.data.Length} bytes from {eventArguments.from}");
		}

		[ContextMenu("Start Listening")]
		void StartListening()
		{
			_client.Start(_port, true);
			_client.synchronizeWithContext = true;
			_client.DataReceived += DataReceivedHandler;
		}

		[ContextMenu("Stop Listening")]
		void StopListening()
		{
			_client.Stop();
			_client.DataReceived -= DataReceivedHandler;
		}

		[ContextMenu("Send")]
		void Send()
		{
			var data = new byte[Random.Range(1, 128)];
			var destination =
				new IPEndPoint(
					new IPAddress(
						_targetIP),
					_targetPort);
			print($"Sending {data.Length} bytes from {_client.listeningPort} to {destination}");
			_client.Send(
				data,
				destination);
		}
	}
}