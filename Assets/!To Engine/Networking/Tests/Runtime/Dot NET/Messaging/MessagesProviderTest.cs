
using System;
using System.Net;
using UnityEngine;
using UnityEngine.Events;
using ICD.Networking.Providing;
using ICD.Networking.Providing.RawData;
using ICD.Networking.Providing.Messaging.Messages;
using ICD.Networking.Tests.Providing.Messaging.Messages;

namespace ICD.Networking.Tests.Providing.Messaging
{
	public class MessagesProviderTest : MonoBehaviour
	{
		[SerializeField]
		private ushort _port;

		[SerializeField]
		private byte[] _targetIP = new byte[] { 127, 0, 0, 1 };
		[SerializeField]
		private short _targetPort = 22222;

		protected IPEndPoint destination =>
			new IPEndPoint(
				new IPAddress(
					_targetIP),
				_targetPort);

		protected Networking.Providing.Messaging.Interfaces.MessagesProvider<IPEndPoint> _messenger =
			new Networking.Providing.Messaging.MessagesProvider<IPEndPoint>(
				new UDPDataProvider() { canSendWhileNotListening = false });

		[SerializeField]
		private UnityEvent _onMessageReceived;

		void DataReceivedHandler(object sender, DataReceivedEventArguments<Message, IPEndPoint> eventArguments)
		{
			print(eventArguments);
			_onMessageReceived?.Invoke();
		}
		void CustomMessageHandler(object sender, DataReceivedEventArguments<TestMessage, IPEndPoint> eventArguments)
		{
			print(eventArguments);
		}

		[ContextMenu("Start Listening")]
		private void StartListeningAction() =>
			StartListening();

		protected virtual void StartListening()
		{
			_messenger.synchronizeWithContext = true;
			_messenger.Start(_port, true);
			_messenger.DataReceived += DataReceivedHandler;
			_messenger.AddMessageHandler<TestMessage>(CustomMessageHandler);
		}

		[ContextMenu("Stop Listening")]
		private void StopListeningAction() =>
			StopListening();
		protected virtual void StopListening()
		{
			_messenger.Stop();
			_messenger.DataReceived -= DataReceivedHandler;
			_messenger.RemoveMessageHandler<TestMessage>(CustomMessageHandler);
		}

		[ContextMenu("Send")]
		protected virtual void Send()
		{
			_messenger.Send(
				new TestMessage(),
				destination);
		}
		protected virtual void OnDisable()
		{
			StopListening();
		}
	}
}