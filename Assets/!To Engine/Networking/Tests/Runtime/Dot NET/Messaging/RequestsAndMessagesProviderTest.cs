
using System.Net;
using System.Threading;
using UnityEngine;
using UnityEngine.Events;
using ICD.Networking.Providing;
using ICD.Networking.Providing.RawData;
using ICD.Networking.Providing.Messaging;
using ICD.Networking.Providing.Messaging.Interfaces;
using ICD.Networking.Tests.Providing.Messaging.Messages;

namespace ICD.Networking.Tests.Providing.Messaging
{
	[ExecuteAlways]
	public class RequestsAndMessagesProviderTest : MessagesProviderTest
	{
		[SerializeField]
		private UnityEvent
			_onRequestReceived,
			_onResponseReceived;

		[SerializeField]
		[Range(.1f, 3600f)]
		private float _timeout;

		private RequestsProvider<IPEndPoint> _requestsProvider;

		public RequestsAndMessagesProviderTest()
		{
			_messenger =
				new RequestsAndMessagesProvider<IPEndPoint>(
					new UDPDataProvider()
					{
						canSendWhileNotListening = false
					});
			_requestsProvider = _messenger as RequestsProvider<IPEndPoint>;
		}
		TestResponse TestRequestHandler(object sender, DataReceivedEventArguments<TestRequest1, IPEndPoint> requestArguments)
		{
			print("Request handler 1");
			_onRequestReceived?.Invoke();
			return new TestResponse();
		}
		TestResponse TestRequestHandler(object sender, DataReceivedEventArguments<TestRequest2, IPEndPoint> requestArguments)
		{
			print("Request handler 2");
			_onRequestReceived?.Invoke();
			return new TestResponse();
		}
		protected override void StartListening()
		{
			base.StartListening();
			_requestsProvider.AddRequestHandler<TestRequest1, TestResponse>(TestRequestHandler);
			_requestsProvider.AddRequestHandler<TestRequest2, TestResponse>(TestRequestHandler);
			_requestsProvider.networkSideIdentificator =
				new ConstantEndPointIdentificator<IPEndPoint>(
					destination);
		}
		protected override void StopListening()
		{
			base.StopListening();
			_requestsProvider.RemoveRequestHandler<TestRequest1, TestResponse>(TestRequestHandler);
			_requestsProvider.RemoveRequestHandler<TestRequest2, TestResponse>(TestRequestHandler);
		}

		[ContextMenu("Send Request 1")]
		void SendRequest1() =>
			SendTestRequest<TestRequest1>();

		[ContextMenu("Send Request 2")]
		void SendRequest2() =>
			SendTestRequest<TestRequest2>();
		async void SendTestRequest<RequestType>()
			where RequestType : TestRequest1, new()
		{
			var response = await _requestsProvider.SendRequest(
				new RequestType(),
				destination,
				new CancellationTokenSource(
					(int)(_timeout * 1000)).Token);

			print($"Response received: {response}");
			if (response)
				_onResponseReceived?.Invoke();
		}
	}
}