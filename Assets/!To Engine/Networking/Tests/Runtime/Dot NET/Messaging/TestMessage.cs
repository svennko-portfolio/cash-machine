
using System;
using ICD.Networking.Providing.Messaging.Messages;

namespace ICD.Networking.Tests.Providing.Messaging.Messages
{
	[Serializable]
	public class TestMessage : Message
    {
		public override string ToString() =>
			"This is the test message";
	}
}
