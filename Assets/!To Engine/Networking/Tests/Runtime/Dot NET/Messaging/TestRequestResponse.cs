
using System;
using ICD.Networking.Providing.Messaging.Messages;

namespace ICD.Networking.Tests.Providing.Messaging.Messages
{
	[Serializable]
    public class TestRequest1 : Request<TestResponse>
    {
		public override string ToString() =>
			"This is the test request 1";
	}

	[Serializable]
	public class TestRequest2 : TestRequest1
    {
		public override string ToString() =>
			"This is the test message";
	}

	[Serializable]
	public class TestResponse : Response
    {
		public override string ToString() =>
			"This is the test response";
	}
}
